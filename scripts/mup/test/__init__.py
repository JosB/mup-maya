"""
MUP - Maya Unity Pipeline
@description Test module
@author Jos Balcaen
@date 24/04/2015
@note 
	There shouldn't be any links to this module.
	Module will not be shipped

@TODO
    - Test for crash handler
    - Sending email
"""
import unittest

from install import InstallTestCase
from creation import CreationTestCase
from settings import SettingsTestCase

import pymel.core as pm


def suite_results(suite):
    """
    Get all the results from a test suite @TODO Do we want to use this?
    """
    ans = {}
    for test in suite:
        if 'results' in test.__dict__:
            ans.update(test.results)
    return ans

def gRun(ui=True):

	suite = unittest.TestSuite()
	suite.addTest(InstallTestCase())
	suite.addTest(CreationTestCase())
	suite.addTest(SettingsTestCase())
	result = unittest.TextTestRunner(verbosity=2).run(suite)

	if ui:
		if not result.wasSuccessful():
			pm.confirmDialog(title='MUP Tests', message='Tests failed, check output dialog for more information')

		else:
			pm.confirmDialog(title='MUP Tests', message='Tests succeeded')

	return result