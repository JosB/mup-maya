"""
MUP - Maya Unity Pipeline
@description General functionality for the tests
@author Jos Balcaen
@date 25/10/2014
"""

import unittest
import mup
import os
import mup.data
import inspect, os

class StartClearTest(unittest.TestCase):
	"""Test where we start clear"""
	def setUp(self):
		""""""

		# Set a blanc options file
		self.mOptionsFile = mup.gData().mOptions.mOptions.mOptionsFile
		self.mOptionsFileBackup = os.environ['APPDATA']+'\\mup\\MUPOptionsBackup.ini'
		
		# Delete original
		if os.path.exists(self.mOptionsFileBackup):
			os.remove(self.mOptionsFileBackup)

		# Set new options files
		os.rename(self.mOptionsFile, self.mOptionsFileBackup)
		
		# Delete original
		if os.path.exists(self.mOptionsFile):
			os.remove(self.mOptionsFile)

		# Clear the data object
		mup.gData().__class__._instances.clear()


	def tearDown(self):
		"""Restore preferences"""

		# Delete original
		if os.path.exists(self.mOptionsFile):
			os.remove(self.mOptionsFile)

		# Set new options files
		os.rename(self.mOptionsFileBackup, self.mOptionsFile)

		# Clear the data object
		mup.gData().__class__._instances.clear()


class EmptyProjectTest(StartClearTest):

	def setUp(self):
		"""@note Will be executed before the test is run"""
		# Call the base class
		super(EmptyProjectTest, self).setUp()

		data = mup.gData()

		
		#inspect.getfile(inspect.currentframe()) # script filename (usually with path)
		folder = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) # script directory

		data.mProjectFolder = folder + '/test_project/'

		data.mWorkFolderName = "work_files"
		data.mGameFolderName = "game_assets"
		
		self.mFilesAndFolders = self.getFilesAndFolders(data.mProjectFolder)
		
		data.save()

	def tearDown(self):
		"""Restore preferences
		@note Will be executed at the end of the test"""

		data = mup.gData()

		# Delete all created files
		created_data = [item for item in self.getFilesAndFolders(data.mProjectFolder) if item not in self.mFilesAndFolders]
		for path in reversed(sorted(created_data)):
			try:
				if os.path.isdir(path):
					os.rmdir(path)
				else:
					os.remove(path)
			except:
				mup.gWarning('Failed to remove: %s' % path)

		# Call the base class
		super(EmptyProjectTest, self).tearDown()

	def getFilesAndFolders(self, inPath):

		out_list = []

		# Get a list of all files
		for dirname, dirnames, filenames in os.walk(inPath):
			for subdirname in dirnames:
				out_list.append(os.path.join(dirname, subdirname))
			for filename in filenames:
				out_list.append(os.path.join(dirname, filename))

		return out_list
