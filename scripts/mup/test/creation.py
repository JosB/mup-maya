"""
MUP - Maya Unity Pipeline
@description Test for the creation of an asset
@author Jos Balcaen
@date 25/10/2014
"""

import mup
from mup.test.general import EmptyProjectTest

import pymel.core as pm

class CreationTestCase(EmptyProjectTest):

	def runTest(self):
		"""Test the creation of an object"""
		
		# Create a cube
		cube = pm.polyCube()[0]
		cube.rename('TestCube')

		# Open the asset creator window
		creator_window = mup.gCreator()

		# Make sure the window is visible
		self.assertEqual(
			creator_window.isVisible(),
			True, 'Failed to create asset creator window'
		)

		# Create a new category
		self.assertEqual(
			creator_window.mManager.createCategory('TestCategory'),
			True, 'Failed to create category'
		)

		creator_window.refreshCategories()

		# Create the asset
		creator_window.ui.mLineEditNameQT.setText('TestAsset')
		self.assertEqual(creator_window.createAsset(), True, 'Failed to create the asset')

		# Check if the window is closed
		self.assertEqual(creator_window.isVisible(), False, 'The creator window is still open')