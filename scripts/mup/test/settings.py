"""
MUP - Maya Unity Pipeline
@description Test for settings
@author Jos Balcaen
@date 24/04/2015
"""

import mup
from mup.test.general import EmptyProjectTest

import pymel.core as pm

class SettingsTestCase(EmptyProjectTest):
	def runTest(self):
		"""Set settings some settings, settings window"""

		# Open the settings window
		settings_window = mup.gSettings()

		# Make sure the window is visible
		self.assertEqual(settings_window.isVisible(), True, 'Failed to create settings window')

		# Set a setting
		self.assertEqual(
			settings_window.mManager.updateSetting('mPrefixModel', 'TEST'),
			True, 'Failed to set settings'
		)

		settings_window.mManager.saveData()

		# Check if setting was set correctly
		self.assertEqual(
			mup.gData().mPrefixModel,
			'TEST', 'Failed to save settings'
		)
		settings_window.close()
