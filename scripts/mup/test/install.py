"""
MUP - Maya Unity Pipeline
@description Test for install of MUP
@author Jos Balcaen
@date 25/10/2014
"""

import mup
from mup.test.general import EmptyProjectTest

class InstallTestCase(EmptyProjectTest):

	def runTest(self):
		"""
		This test will go through the install window
		"""
		install_window = mup.gInstall()

		# Make sure the window is visible
		self.assertEqual(install_window.isVisible(), True, 'Failed to create install window')

		# Go the the location tab
		self.assertEqual(install_window.next(), True, 'Failed to go to the next install tab, failed welcome')

		# Go the the interface tab
		self.assertEqual(install_window.next(), True, 'Failed to go to the next install tab, failed location')

		# Go the the done tab
		self.assertEqual(install_window.next(), True, 'Failed to go to the next install tab, failed interface')

		# Should close
		self.assertEqual(install_window.next(), True, 'Failed to succeed the install process')
		
		# Make sure the window is closed
		self.assertEqual(install_window.isVisible(), False, 'Failed to end the install processs')
