"""
MUP - Maya Unity Pipeline
@description Asset class
@author Jos Balcaen
@date 19/04/2014
"""

import os

from PySide import QtGui, QtCore
import pymel.core as pm

import mup
import mup.exporter
from mup.system import general
from mup.system.log import LogItem
from mup.system import decorators


class Asset(object):
	"""Asset object """

	def __init__(self, inFolder, inParseData=True):
		"""
		Create a new asset object
		@param inFolder (string) Folder where the objects lives. @TODO Make relative
		@param inParseData (boolean) Parse & populate all data. Default: True
		"""
		self.mFolder		= inFolder
		self.mMayaFile		= general.gGonvertFolderToWorkFilePath(inFolder)
		self.mExportFile	= general.gConvertWorkFileToExportPath(self.mMayaFile)
		self.mMupFile		= general.gConvertPathToMupXML(self.mMayaFile)

		self.mAuthor = None
		self.mLastModifiedDate = None
		self.mLastModifiedAuthor  = None
		self.mInfo = ""
		self.__mTags = []
		self.mLogs = []

		# Populate the asset info
		if inParseData: self.readAssetInfo()

	@property
	def mParentFolder(self):
		"""Parent folder of this asset"""
		return os.path.split(self.mFolder[:-1])[0] + '/'

	@property
	def mGameFolder(self):
		"""Game folder of this asset"""
		return mup.system.general.gConvertWorkToGame(self.mFolder)

	@property
	def mName(self):
		return os.path.basename(self.mFolder)

	def getTags(self):
		return self.__mTags

	def setTags(self, inValue):
		if isinstance(inValue, basestring) and ',' in inValue:
			self.__mTags = [x.strip() for x in inValue.split(',')]
			return
		if isinstance(inValue, list):
			self.__mTags = inValue
			return
		
		raise Exception('Invalid value: %s' % inValue)

	mTags = property(getTags, setTags)

	def readAssetInfo(self):
		"""
		Read the metadata of the asset
		@return (boolean) True if data parsed fine
		"""

		# Check if the file exists
		if not os.path.exists(self.mMupFile): return False
		
		# inline help method to get data from a line
		def getDataFromLine(inLine): return inLine[inLine.index('=')+1:].strip()

		# Open the file and populate the data
		with open(self.mMupFile) as file:
			for line in file:
				# @TODO Define these tags in globals
				if line.startswith('#author'):					self.mAuthor = getDataFromLine(line)
				elif line.startswith('#lastmodifieddate'):		self.mLastModifiedDate = getDataFromLine(line)
				elif line.startswith('#lastmodifiedauthor'):	self.mLastModifiedAuthor  = getDataFromLine(line)
				elif line.startswith('#tags'):					self.mTags = getDataFromLine(line)
				elif line.startswith('#end'):
					return True

		mup.gWarning('End tag, %s not found in mup file "%s"' % ('#end', self.mMupFile))
		return False


	def addLog(self, inLogItem):
		"""
		Log an item
		@param inLogItem (LogItem)
		"""
		self.mLods.append(inLogItem)


	@decorators.gResult
	def validateAsset(self):
		"""
		Validate this asset
		@return (Result)
		"""

		#@TODO Test this function

		# Check work file
		maya_file = mup.gData().mPrefixModel + self.mName + '.%s' % mup.gData().mExtensionWork
		if os.path.basename(self.mMayaFile) != maya_file:
			self.validateAsset.addLog(
				self.validateAsset.LogItem(
					label='Maya file %s has wrong name. Should be %s' % (self.mMayaFile, maya_file),
					level=self.validateAsset.ELogLevel.ERROR
				)
			)

		# Check game file
		export_file = mup.gData().mPrefixModel + self.mName + '.%s' % mup.gData().mExtensionExport
		if os.path.basename(self.mExportFile) != maya_file:
			self.validateAsset.addLog(
				self.validateAsset.LogItem(
					label='Maya file has wrong name. Should be %s' % maya_file,
					level=self.validateAsset.ELogLevel.ERROR
				)
			)

		# Result is returned by the decorator


	def export(self, inRestoreScene=True):
		"""
		Export this asset
		@param inRestoreScene (boolean) Restore original scene. Default: True
		@return (Result)
		"""
		return mup.exporter.gExport(asset=self, restore=inRestoreScene)
		

	def open(self):
		"""Open this asset in Maya"""
		general.gOpenScene(self.mMayaFile)


	def importAsset(self, **kwargs):
		"""
		Import the asset in the current scene
		@param inGame (boolean) Game data. Default: True
		@param inWork (boolean) Work data. Default: not inGame
		"""
		inGame = kwargs.setdefault('inGame', True)
		inWork = kwargs.setdefault('inWork', not inGame)

		file = self.mMayaFile if inWork else pm.importFile(self.mExportFile)
		
		# Check if file exists
		if not os.path.isfile(file):
			mup.gWarning('File does not exists: %s' % file)
			return false

		pm.importFile(file)


	@decorators.gResult
	def delete(self, inWorkFolder=True, inGameFolder=False):
		"""
		Delete this asset
		@param inWorkFolder (boolean) Delete work folder. Default: True
		@param inGameFolder (boolean) Delete game folder. Default: True
		@return (Result)
		"""
		# Delete the disk data
		self.delete.merge(general.gDeleteFolder(self.mFolder, inForce=True))
		self.delete.merge(general.gDeleteFolder(general.gConvertWorkToGame(self.mFolder), inForce=True))

		# Result is returned by the decorator


class AssetWidgetItem(Asset,QtGui.QListWidgetItem):
	"""
	WidgetItem - Used for the browser
	@derived (Asset|QListWidgetItem)
	"""

	def __init__(self, inFolder, inParent=None):
		"""Create a new asset widget item
		@param inFolder (string) Path to the file
		@param inParent (QWidget)
		"""

		QtGui.QListWidgetItem.__init__(self)
		Asset.__init__(self, inFolder)

		self.mThumbFile = inFolder + '/' + mup.gData().mPrefixThumb + self.mName + '.jpg'
		self.setText(self.mName)
		
		# Set the icon file
		self.updateIcon()
		

	def grabViewport(self):
		"""Grab the viewport as thumb for this asset"""
		import maya.OpenMaya as api # @TODO Change this to PyMel calls
		import maya.OpenMayaUI as apiUI

		pm.displayRGBColor('background', 1, 0, 0)
		# Grab the last active 3d viewport
		view = apiUI.M3dView.active3dView()

		# Read the color buffer from the view, and save the MImage to disk
		image = api.MImage()
		view.readColorBuffer(image, True)
		size = mup.gData().mIconSize
		image.resize(size, size, True)
		image.writeToFile(self.mThumbFile, 'jpg')

		self.updateIcon()


	def updateIcon(self):
		"""Set the icon from the file"""
		
		pixmap = None
		if os.path.exists(self.mThumbFile):
			pixmap = QtGui.QPixmap(self.mThumbFile)
		else:
			# Set default icon
			pixmap = QtGui.QPixmap(mup.gData().mUiFolder+'icon.png')

		# Resize pixmap
		size = mup.gData().mIconSize
		pixmap = pixmap.scaled( 
			QtCore.QSize(size, size), QtCore.Qt.KeepAspectRatioByExpanding, QtCore.Qt.SmoothTransformation
		)

		# Crop the image if necessary
		if pixmap.width() != size:
			crop_offset_x = (pixmap.width() - size) / 2
			pixmap = pixmap.copy(crop_offset_x, 0, size, size)

		# Apply the cropped icon
		self.setIcon(pixmap)