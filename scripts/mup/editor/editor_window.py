"""
MUP - Maya Unity Pipeline
@description Editor Window
@author Jos Balcaen
@date 24/04/2015
"""
import mup
from mup.ui.basewindow import BaseWindow
from mup.system import general, gError
from PySide import QtGui, QtCore

from functools import partial


class AssetEditorWindow(BaseWindow):
	"""Asset editor window"""
	def __init__(self, inManager):
		BaseWindow.__init__(self)

		self.mManager = inManager
		self.mTitle = 'MUP - Asset Editor'
		self.mEnableHelp = True
		self.mUiWidgetFile = self.mManager.mData.mWidgetFolder + 'asset_editor.ui'


class SettingsEditorWindow(BaseWindow):
	"""Settings editor window"""
	def __init__(self, inManager):
		BaseWindow.__init__(self)

		self.mManager = inManager
		self.mTitle = 'MUP - Settings Editor'
		self.mEnableHelp = True
		self.mUiWidgetFile = self.mManager.mData.mWidgetFolder + 'settings_editor.ui'
		
		self.mOriginalData = mup.gData()
		self.mData = mup.gData()
		

	def additionalUiSetup(self):
		# Display the data
		self.fillInData()

		# Connections
		self.ui.mPushButtonSaveQT.clicked.connect(self.mManager.saveData)
		self.ui.mPushButtonReloadQT.clicked.connect(self.reloadData)
		self.ui.mPushButtonResetQT.clicked.connect(self.resetData)

		def createSetFunction(inAttrName, inType=None):
			"""Small help function to create a partial object"""
			return partial(self.mManager.updateSetting, inAttrName, inType=inType)

		# Note: This breaks if names have changed in the date object
		self.ui.mLineEditProjectFolderQT.textChanged.connect(	createSetFunction('mProjectFolder')	)

		self.ui.mLineEditWorkFolderQT.textChanged.connect(		createSetFunction('mWorkFolderName')	)
		self.ui.mLineEditGameFolderQT.textChanged.connect(		createSetFunction('mGameFolderName')	)
		self.ui.mLineEditModelFolderQT.textChanged.connect(		createSetFunction('mModelFolderPath')	)
																			
		self.ui.mLineEditPrefixThumbQT.textChanged.connect(		createSetFunction('mPrefixThumb')	)
		self.ui.mLineEditPrefixModelQT.textChanged.connect(		createSetFunction('mPrefixModel')	)
		self.ui.mLineEditWorkTypeQT.textChanged.connect(		createSetFunction('mWorkType')		)
		self.ui.mLineEditExtensionWorkQT.textChanged.connect(	createSetFunction('mExtensionWork')	)
		self.ui.mLineEditExportTypeQT.textChanged.connect(		createSetFunction('mExportFormat')	)
		self.ui.mLineEditExtensionExportQT.textChanged.connect(	createSetFunction('mExtensionExport'))
																			
		self.ui.mCheckBoxShowAssetDetailsQT.stateChanged.connect(createSetFunction('mShowAssetDetails', inType=QtCore.Qt.CheckState))
		self.ui.mSpinBoxIconSizeQT.valueChanged.connect(		createSetFunction('mIconSize'))

		self.ui.mCheckBoxDebugModeQT.stateChanged.connect(		createSetFunction('mDebugMode', inType=QtCore.Qt.CheckState))


	def resetData(self, *args):
		"""Reset all values to the default values"""
		self.mData.mOptions.reset()
		self.fillInData()


	def reloadData(self, *args):
		"""Re-read the data from the file"""
		self.mData.mOptions.reload()
		self.fillInData()


	def fillInData(self):
		"""Display the data in the interface"""
		self.ui.mLineEditProjectFolderQT.setText(	self.mData.mProjectFolder)
		
		self.ui.mLineEditWorkFolderQT.setText(		self.mData.mWorkFolder)
		self.ui.mLineEditGameFolderQT.setText(		self.mData.mGameFolder)
		self.ui.mLineEditModelFolderQT.setText(		self.mData.mModelFolder)

		self.ui.mLineEditPrefixThumbQT.setText(		self.mData.mPrefixThumb)
		self.ui.mLineEditPrefixModelQT.setText(		self.mData.mPrefixModel)
		self.ui.mLineEditWorkTypeQT.setText(		self.mData.mWorkType)
		self.ui.mLineEditExtensionWorkQT.setText(	self.mData.mExtensionWork)
		self.ui.mLineEditExportTypeQT.setText(		self.mData.mExportFormat)
		self.ui.mLineEditExtensionExportQT.setText(	self.mData.mExtensionExport)

		self.ui.mCheckBoxShowAssetDetailsQT.setCheckState(QtCore.Qt.Checked if self.mData.mShowAssetDetails else QtCore.Qt.Unchecked)
		self.ui.mSpinBoxIconSizeQT.setValue(		self.mData.mIconSize)

		self.ui.mCheckBoxDebugModeQT.setCheckState(	QtCore.Qt.Checked if self.mData.mDebugMode else QtCore.Qt.Unchecked)


	def createEditActions(self):
		"""Method to add actions in the edit menu"""
		
		open_options_file_action = QtGui.QAction(QtGui.QIcon('open.png'), '&Open Options File', self)        
		#exitAction.setShortcut('Ctrl+O')
		open_options_file_action.setStatusTip('Open the option file where the data is saved')
		open_options_file_action.triggered.connect(self.mManager.openOptionsFile)

		self.menuEdit.addAction(open_options_file_action)


	