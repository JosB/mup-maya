"""
MUP - Maya Unity Pipeline
@description Editor
@author Jos Balcaen
@date 19/08/14
"""
import mup

from editor_window import AssetEditorWindow, SettingsEditorWindow
from mup.system import gError
from PySide import QtCore


class AssetEditorManager(object):
	"""Class containing all functionality to edit an asset"""
	def __init__(self, inAsset):
		self.mData = mup.gData()
		self.mAsset = inAsset


class SettingsEditorManager(object):
	"""Class containing all functionality to edit project settings settings"""
	def __init__(self):
		self.mData = mup.gData()


	def saveData(self, *args):
		"""Write the data to the options file"""
		self.mData.save()


	def updateSetting(self, inName, inValue, inType=None):
		"""Update a value from the data
		@param inName (string) Attribute name
		@param inValue (object) Attribute value
		@param inType (Class) Only used for QtCore.Qt.CheckState"""

		if inType == QtCore.Qt.CheckState:
			inValue = (inValue == QtCore.Qt.Checked)

		try:
			setattr(self.mData, inName, inValue)
			return True
		except AttributeError as err:
			gError('Failed to set %s for %s in settings: %s' % (inValue, inName, err))
			return False


	def openOptionsFile(self, *args):
		"""Open the options file"""
		mup.system.general.gOpenFile(self.mData.mOptions.mOptions.mOptionsFile)


def gShowAssetEditor(inAsset):
	"""Show the asset editor
	@return (AssetEditorWindow) The created window"""

	manager = AssetEditorManager(inAsset)
	w = AssetEditorWindow(manager)
	w.showUi()
	return w


def gShowSettingsEditor():
	"""Show the options editor
	@return (SettingsEditorWindow) The created window"""

	manager = SettingsEditorManager()
	w = SettingsEditorWindow(manager)
	w.showUi()
	return w