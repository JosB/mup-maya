"""
MUP - Maya Unity Pipeline
@description Editor module
@author Jos Balcaen
@date 20/08/2014
"""
from editor import gShowAssetEditor, gShowSettingsEditor