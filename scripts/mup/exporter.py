"""
MUP - Maya Unity Pipeline

@description

	All functionality to export an object
	- Export Maya file to FBX or MB/MA 
	- Write MUP XML file

@author Jos Balcaen
@date 25/10/2014
"""

import datetime, os
from xml.dom import minidom

import pymel.core as pm

import mup
from mup.system import general, attributes, globals, decorators

@decorators.gResult
def gExport(**kwargs):
	"""
	Export the given asset
	@param asset (Asset) Asset to export
	@param xml (boolean) Export the XML file. Default: True
	@param restore (boolean) Restore the original scene. Default: settings value
	@return (Result) Result object returned by the decorator
	"""

	# Parse arguments
	inAsset = kwargs.setdefault("asset")
	inExportXml = kwargs.setdefault("xml", True)
	inRestore = kwargs.setdefault("restore", mup.gData().mExportXML)
	
	original_scene_path = pm.sceneName()

	# Open the file if not already open
	general.gOpenScene(inAsset.mMayaFile)

	# Get all visible objects
	visible_objects = pm.ls(visible=True, long=True, dagObjects=True, type=pm.nt.Transform)
	
	# Write the XML file
	if inExportXml: 
		result = gWriteXML(asset=inAsset)
		# Merge the result (make it non fatal if XML writing fails) 
		# @TODO We probably don't want this in the later stage, but at the moment mup files are not used that extensively
		gExport.merge(result.errorsToWarning())

	# Get all the objects that need to be exported
	objects_to_export = [obj for obj in visible_objects if _needsToBeExported(obj)]

	# Export the file
	format = mup.gData().mExportFormat

	if format == globals.sExportFormatMaya:
		gExport.merge(_exportMaya(objects_to_export, inAsset.mExportFile))

	elif format == globals.sExportFormatFBX:
		gExport.merge(_exportFBX(objects_to_export, inAsset.mExportFile))

	else:
		# Log error, but proceed
		gExport.addLog(gExport.LogItem(label='Unsupported Export Format: "%s"' % format, level=gExport.ELogLevel.ERROR))

	# Restore the original open scene
	if inRestore:
		general.gOpenScene(original_scene_path, askForSave=False)

	# @note The result is automatically returned by the decorator


def gExportThisScene():
	"""
	@TODO At the moment exporting is only possible from the browser
	"""
	raise NotImplementedError('@TODO')


@decorators.gResult
def gWriteXML(**kwargs):
	"""
	Write the XML of the given asset
	@param asset (Asset) Asset
	"""
	inAsset = kwargs.setdefault("asset")

	# Open the file if not already open
	general.gOpenScene(inAsset.mMayaFile)

	# Get all visible objects
	visible_objects = pm.ls(visible=True, long=True, dagObjects=True, type=pm.nt.Transform)
	
	# [dict('name':obj name, 'material':..)]
	object_dict_array = []

	for obj in visible_objects:
		# Check if object needs to be exported
		if _needsToBeExported(obj) == False: continue

		# Get all information from object and save in dict
		obj_dict = {}
		obj_dict['name'] = obj.shortName()

		# Get material 
		shape = obj.getShape()

		if shape != None:
			# Get the shaders
			shading_grps= shape.listConnections(type=pm.nt.ShadingEngine)
			shaders = [shading_grp.listConnections(type=pm.nt.ShadingDependNode)[0] for shading_grp in shading_grps]

			obj_dict['material'] = list(set(shaders))

		else:
			obj_dict['material'] = None
		
		# Get mup attributes
		for attr in attributes.gGetAttributes():
			if hasattr(obj, attr.mLongName):
				value = getattr(obj, attr.mLongName).get()
				obj_dict[attr.mSaveName] = value
			else:
				text = 'Object "%s" doesn\'t have attribute "%s"' % (obj, attr.mLongName)
				gWriteXML.addLog(gWriteXML.LogItem(label=text, level=gWriteXML.ELogLevel.WARNING, node=obj))
		
				obj_dict[attr.mLongName] = attr.mDefaultValue
		
		# Store dict
		object_dict_array.append(obj_dict)

	# Create a new XML document
	doc = minidom.Document()
	root = doc.createElement("mup")

	# Save all the attributes in the object dict
	for obj_dict in object_dict_array:
		object_node = doc.createElement('object')

		for key, value in obj_dict.items():
			temp = doc.createElement(key)
			txt = doc.createTextNode('%s' % value)
			temp.appendChild(txt)
			object_node.appendChild(temp)

		root.appendChild(object_node)

	# Add the meta data as a comment
	comment = doc.createComment(_createMetaDataString(inAsset))
	
	# Link together
	doc.appendChild(comment)
	doc.appendChild(root)

	try:
		# Create the file if it doesn't exists
		if not os.path.isfile(inAsset.mMupFile):
			
			temp = open(inAsset.mMupFile, 'w')
			temp.write('')
			temp.close()

		# Write the XML data to the file
		doc.writexml(
			open(inAsset.mMupFile, 'w'),
			indent='  ',
			addindent='  ',
			newl='\n'
		)

	except IOError as ex: 
		gWriteXML.addLog(gWriteXML.LogItem(label='Failed to write to %s:%s' % (inAsset.mMupFile, ex), level=gWriteXML.ELogLevel.ERROR))

	doc.unlink()


def _needsToBeExported(inObject):
	"""Check if the given object needs to be exporter
	@param inObject (PyNode)"""
	# Check for export attribute

	if hasattr(inObject, globals.sAttrExport):
		value = getattr(inObject, globals.sAttrExport).get()
		if value == False: return False

	
	# Check if one of the parents are marked not to export
	for parent in inObject.getAllParents():
		if hasattr(parent, globals.sAttrExport):
			value = getattr(parent, globals.sAttrExport).get()
			if value == False: return False

	return True


@decorators.gResult
def _exportFBX(inObjects, inPath):
	"""Export the given objects to fbx
	@param inObjects ([PyNode]) Objects to export
	@param inPath (string)
	@return (Result) Returned by the decorator
	"""
	# Don't export if length of inObjects is zero
	if not len(inObjects): return

	pm.select(inObjects, replace=True)

	# Make sure the fbx plug-in is loaded
	if not pm.pluginInfo(globals.sFBXPlugin, query=True, loaded=True):
		pm.loadPlugin(globals.sFBXPlugin)

	# FBX Exporter options. Set as required.
	# You can find a reference guide here: http://download.autodesk.com/us/fbx/20112/Maya/_index.html
	# Just add/change what you need.

	# Geometry
	pm.mel.FBXExportSmoothingGroups(v=True)
	pm.mel.FBXExportHardEdges(v=False)
	pm.mel.FBXExportTangents(v=True)
	pm.mel.FBXExportSmoothMesh(v=False)
	pm.mel.FBXExportInstances(v=False)
	pm.mel.FBXExportReferencedAssetsContent(v=False)

	# Animation
	"""
	mel.eval("FBXExportBakeComplexAnimation -v false")
	#mel.eval("FBXExportBakeComplexStart -v "+str(exportStartFrame[x]))
	#mel.eval("FBXExportBakeComplexEnd -v "+str(exportEndFrame[x]))
	mel.eval("FBXExportBakeComplexStep -v 1")
	#mel.eval("FBXExportBakeResampleAll -v true")
	"""
	pm.mel.FBXExportUseSceneName(v=False)
	pm.mel.FBXExportQuaternion(v="euler")
	pm.mel.FBXExportShapes(v=True)
	pm.mel.FBXExportSkins(v=True)

	# Constraints
	pm.mel.FBXExportConstraints(v=False)
	# Cameras
	pm.mel.FBXExportCameras(v=False)
	# Lights
	pm.mel.FBXExportLights(v=False)
	# Embed Media
	pm.mel.FBXExportEmbeddedTextures(v=False)
	# Connections
	pm.mel.FBXExportInputConnections(v=False)
	# Axis Conversion
	pm.mel.FBXExportUpAxis("y")

	# Export!
	file_name, file_extension = os.path.splitext(inPath)

	d = os.path.dirname(file_name)
	if not os.path.exists(d):
		os.makedirs(d)

	pm.mel.FBXExport(f=("%s.fbx" % file_name), s=True)


@decorators.gResult
def _exportMaya(inObjects, inPath):
	"""
	Export the given objects to Maya
	@param inObjects ([PyNode]) Objects to export
	@param inPath (string)
	@return (Result) Returned by the decorator
	"""
	# Rename the scene
	original_scene_name = pm.sceneName()
	pm.renameFile(inPath)
	pm.select(inObjects, replace=True)
	
	# Save Maya game file
	pm.exportSelected(
		force				= True,
		type				= globals.sTypeMayaBinary,
		constructionHistory = False
	)

	# Restore scene name
	pm.renameFile(rename=original_scene_name)


def _createMetaDataString(inAsset):
	"""
	Create a meta data string for the given asset.
	This string is used to get a lightweight representation of the asset.
	@param inAsset (Asset)
	@return (string)
	"""
	# @TODO Save tags in global variables

	comment_string = "\n#metadata\n"
	
	comment_string += "#author = %s\n" % inAsset.mAuthor
	today = datetime.date.today()
	comment_string += "#lastmodifieddate = %s\n" % (today.strftime('%d/%m/%y'))
	comment_string += "#lastmodifiedauthor = %s\n" % mup.gData().mUser
	comment_string += "#info = %s\n" % inAsset.mInfo
	
	comment_string += '#end\n'
	return comment_string