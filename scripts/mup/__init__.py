"""
MUP - Maya Unity Pipeline
@description init
@author Jos Balcaen
@date 09/04/2014
"""

'''
----------------------------------------------------------------------------------
StyleGuide
----------------------------------------------------------------------------------

module_name                                        ( mup.browser )
ClassName                                          ( mup.browser.browser.BrowserManager )
objectMethod                                       ( mup.browser.browser.BrowserManager.getAllAssetsInFolder() )
mObjectMemberVariable                              ( mup.browser.browser.BrowserManager.mTypes )
mObjectMemberVariableDefinedInQtDesignerFileQT     ( mup.browser.browser_window.BrowserWindow.ui.mComboBoxTypeQT )
sClassVariableStatic                               ( mup.globals.sExtensionMaya )
sClassMethodStatic                                 ( mup.sample.sIsBrowserVisible() )
gGlobalMethod                                      ( mup.browser.gShow() )
gGlobalVariable                                    ( mup.exporter.gIsExporting )
ENameOfEnumerration                                ( mup.system.log.ELogLevel )
ENUMERATION_VALUE                                  ( mup.system.log.ELogLevel.SUCCES )
inInputParameter                                   ( mup.system.log.gWarning(inMessage)
local_variable                                     ( log_item = self.mLog)

White spaces
Add white spaces to make code more readable

Two empty lines after a function/class
One line after a function definition

Comments
Write file headers. Ex: See top
Write function/class describing headers. 
Add parameter information for functions. Ex: 

def addLog(inMessage, inLevel):
	"""
	Log a message
	@param inMessage (string) Message to be logged
	@param inLevel (ELogLevel) Log level. Ex ELogLevel.WARNING
	@return (boolean) True when succeeded
	"""


Use @TODO tags with more information. This makes it easy to search for tasks that still need to be done
Use @HARDCODED when something is hard coded

----------------------------------------------------------------------------------
Test
----------------------------------------------------------------------------------
Run the tests before you put something in and update them if necessary

import mup.test
mup.test.gRun()

'''

# Import useful function into the global mup namespace
from data import gData
from install import gShow as gInstall
from browser import gShow as gBrowser
from creator import gShow as gCreator
from editor import gShowSettingsEditor as gSettings

from system import gDebug, gError, gInfo, gPrint, gWarning, Result