from mup.install.steps.step import Step
from PySide import QtGui
import mup
import os

#@TODO Uniy path (forward slashes)

class StepLocation(Step):
    """
    Set the location of the project
    @derived (Step) Base class for all install steps"""
    def __init__(self, widget, ui):
        super(StepLocation, self).__init__(widget, ui)

    def setup(self):
        # Project folder
        self.mUi.mPushButtonBrowseProjectFolderQT.clicked.connect(self.browseProjectFolder)
        self.mUi.mLineEditProjectFolderQT.textChanged.connect(self.lineEditProjectFolderTextChanged)

        # Work folder
        self.mUi.mPushButtonBrowseWorkFolderQT.clicked.connect(self.browseWorkFolder)
        self.mUi.mLineEditWorkFolderQT.textChanged.connect(self.lineEditWorkFolderTextChanged)

        # Game folder
        self.mUi.mPushButtonBrowseGameFolderQT.clicked.connect(self.browseGameFolder)
        self.mUi.mLineEditGameFolderQT.textChanged.connect(self.lineEditGameFolderTextChanged)

        # Display image of the folder structure that will be created
        self.mUi.mLabelStructureImageQT.setPixmap(QtGui.QPixmap(self.mData.mUiFolder + 'installFolderStructure'))

        # Pre fill unity project folder if in data
        if self.mData.mProjectFolder: self.mUi.mLineEditProjectFolderQT.setText(self.mData.mProjectFolder)
        if self.mData.mWorkFolder: self.mUi.mLineEditWorkFolderQT.setText(self.mData.mWorkFolder)
        if self.mData.mGameFolder: self.mUi.mLineEditGameFolderQT.setText(self.mData.mGameFolder)

    def browseProjectFolder(self):
        """Popup dialog where the user can select a folder"""
        folder = QtGui.QFileDialog.getExistingDirectory(self.mWidget, "Select project folder", "c:/", QtGui.QFileDialog.ShowDirsOnly)
        if folder == None: return
        else:
            folder = folder.replace('\\','/') + '/'
            # Display folder in lineEdit, this will automatically call the lineEditProjectFolderTextChanged method
            self.mUi.mLineEditProjectFolderQT.setText(folder)

    def browseGameFolder(self):
        """Popup dialog where the user can select the game folder"""
        folder = QtGui.QFileDialog.getExistingDirectory(self.mWidget, "Select Game folder", self.mUi.mLineEditProjectFolderQT.text() or "c:/", QtGui.QFileDialog.ShowDirsOnly)
        if folder == None: return
        else:
            folder = folder.replace('\\','/') + '/'
            # Display folder in lineEdit, this will automatically call the lineEditGameFolderTextChanged method
            self.mUi.mLineEditGameFolderQT.setText(folder)

    def browseWorkFolder(self):
        """Popup dialog where the user can select the work folder"""
        folder = QtGui.QFileDialog.getExistingDirectory(self.mWidget, "Select Work folder", self.mUi.mLineEditProjectFolderQT.text() or "c:/", QtGui.QFileDialog.ShowDirsOnly)
        if folder == None: return
        else:
            folder = folder.replace('\\','/') + '/'
            # Display folder in lineEdit, this will automatically call the lineEditGameFolderTextChanged method
            self.mUi.mLineEditWorkFolderQT.setText(folder)
            

    def execute(self):
        """Method called when pressing the next button"""
        # Validate the folders again
        
        if self.isValidProjectData():
            self.saveFolderData()

        else: 
            return False

        return True

    def saveFolderData(self):
        """Save all folder data to the options file
        @description
            - mProjectFolder
            - mWorkFolder
            - mGameFolder

        """

        self.mData.mProjectFolder = self.mUi.mLineEditProjectFolderQT.text()

        work_folder = self.mUi.mLineEditWorkFolderQT.text()
        if work_folder.endswith('/'): work_folder = work_folder[:-1]

        game_folder = self.mUi.mLineEditGameFolderQT.text()
        if game_folder.endswith('/'): game_folder = game_folder[:-1]

        self.mData.mWorkFolderName = os.path.basename(work_folder)
        self.mData.mGameFolderName = os.path.basename(game_folder)

        # Save project folder + game and work folder name
        self.mData.save()

    def lineEditProjectFolderTextChanged(self, inFolder):
        """Called when text is changed in the mLineEditProjectFolderQT
        @param inFolder (string) Value from the line edit"""
        # User feedback
        if self.gIsValidProjectFolder(inFolder): 
            self.__markLineEditAsValid( self.mUi.mLineEditProjectFolderQT)
            mup.gInfo("Project folder validated.")
        else:
            self.__markLineEditAsUnvalid( self.mUi.mLineEditProjectFolderQT)
            mup.gWarning("Selected folder is not a valid project folder. See script editor for more details.")

    def lineEditWorkFolderTextChanged(self, inFolder):
        """Called when text is changed in the mLineEditWorkFolderQT
        @param inFolder (string) Value from the line edit"""
        # User feedback
        if self.isValidWorkFolder(inFolder): 
            self.__markLineEditAsValid( self.mUi.mLineEditWorkFolderQT)
            mup.gInfo("Work folder validated.")
        else:
            self.__markLineEditAsUnvalid( self.mUi.mLineEditWorkFolderQT)
            mup.gWarning("Selected folder is not a valid work folder. See script editor for more details.")

    def lineEditGameFolderTextChanged(self, inFolder):
        """Called when text is changed in the mLineEditGameFolderQT
        @param inFolder (string) Value from the line edit"""
        # User feedback
        if self.isValidGameFolder(inFolder): 
            self.__markLineEditAsValid( self.mUi.mLineEditGameFolderQT)
            mup.gInfo("Game folder validated.")
        else:
            self.__markLineEditAsUnvalid( self.mUi.mLineEditGameFolderQT)
            mup.gWarning("Selected folder is not a valid game folder. See script editor for more details.")

    def __markLineEditAsValid(self, inLineEdit):
        inLineEdit.setStyleSheet('QWidget {background-color: rgb(25,150,25)}')

    def __markLineEditAsUnvalid(self, inLineEdit):
        inLineEdit.setStyleSheet('QWidget {background-color: rgb(200,25,25)}')


    def isValidFolder(self, inFolder):
        """

        """
        if not os.path.exists(inFolder):
            mup.gWarning("Folder doesn't exist")
            return False
        return True

    def gIsValidProjectFolder(self, inFolder):
        """
        Check if the given folder is a valid project folder
        @description Requirements for a valid project folder:
            - Contains a workfolder
            - Contains a gamefolder (unity project folder)
        @param inFolder (string) Absolute path to the folder
        @return (boolean) valid/not valid
        """
        mup.gDebug("Validating project folder: %s" % inFolder)
        

        if not self.isValidFolder(inFolder): return False

        
        # Check if the folder contains two sub folders
        sub_folders = [(inFolder + '/' + name) for name in os.listdir(inFolder) if os.path.isdir(inFolder + '/' + name)]

        if len(sub_folders) < 2:
            mup.gWarning("The main project folder must contain two subfolders (work and game folder)")
            return False

        # One of those folders should be a Unity project folder
        contains_unity_folder = False
        for sub_folder in sub_folders:
            if self.isValidUnityFolder(sub_folder, silent=False):
                contains_unity_folder = True
        
        if not contains_unity_folder:
            mup.gWarning("The main project folder must contain a game folder with the unity project files inside")
            return False

        # Didn't fail any check, so looks like it's a valid project folder
        return True
        
    def isValidGameFolder(self, inFolder):
        mup.gDebug("Validating game folder: %s" % inFolder)
        

        if not self.isValidFolder(inFolder): return False

        if not self.isValidUnityFolder(inFolder): return False

        return True

    def isValidWorkFolder(self, inFolder):
        mup.gDebug("Validating work folder: %s" % inFolder)
        

        if not self.isValidFolder(inFolder): return False

        return True

    def isValidUnityFolder(self, inFolder, silent=False):
        """
        Check if the given folder is a valid unity folder
        @description Requirements for a valid unity folder:
            - Contains subfolders (Assets, Library, ProjectSettings)
        @param inFolder (string) Absolute path to the folder
        @return (boolean) True if folder is a unity folder
        """
        mup.gDebug("Validating unity folder: %s" % inFolder)

        if not os.path.exists(inFolder):
            if not silent: mup.gWarning("Folder doesn't exist")
            return False

        subfolders = [o.lower() for o in os.listdir(inFolder) if os.path.isdir(inFolder + '/' + o)]
        if not 'assets' in subfolders or not 'library' in subfolders or not 'projectsettings' in subfolders:
            if not silent: mup.gWarning("Failed to validate unity folder")
            return False

        # Didn't fail any check, so looks like it's a valid project folder
        return True

    def isValidProjectData(self):

        if not self.gIsValidProjectFolder(self.mUi.mLineEditProjectFolderQT.text()) or \
            not self.isValidGameFolder(self.mUi.mLineEditGameFolderQT.text()) or \
            not self.isValidWorkFolder(self.mUi.mLineEditWorkFolderQT.text()):

            return False

        return True