from mup.install.steps.step import Step
from mup.system import globals
import pymel.core as pm

class StepInterface(Step):
	def __init__(self, widget, ui):
		super(StepInterface, self).__init__(widget, ui)

		self.mShelfHeight = 33

	def setup(self):
		pass

	def execute(self):

		if self.mUi.mCheckBoxCreateShelfQT.isChecked():
			self.createShelf(inReplace=self.mUi.mCheckBoxReplaceShelfQT.isChecked())

		return True

	def createShelf(self, inReplace=False):
		"""
		Creates a shelf with the title mup in the main shelf
		@param inReplace (boolean) Delete mup shelf if already exists
		@return (boolean) result
		"""
		already_exists = pm.shelfLayout(globals.sShelfName, q=True, exists=True)
		if not inReplace and already_exists: return True

		if already_exists:
			pm.deleteUI(globals.sShelfName)

		# Get $gShelfTopLevel as a python string
		shelf_top_level = pm.mel.eval("$temp=$gShelfTopLevel")

		# Create a new shelf layout
		pm.shelfLayout(globals.sShelfName, cellWidth=self.mShelfHeight, cellHeight=self.mShelfHeight, parent=shelf_top_level)

		# Add buttons to this layout
		pm.shelfButton(annotation='Show browser', image1='mup_browse.png', width=self.mShelfHeight, height=self.mShelfHeight,
			command='import mup.browser; mup.browser.gShow()')

		pm.shelfButton(annotation='Export the scene.', image1='mup_export_asset.png',
			command='import mup.exporter; mup.exporter.gExportThisScene()')
	
		pm.shelfButton(annotation='Open settings', image1='mup_settings.png',
			command='import mup.editor as editor; editor.gShowSettingsEditor()')

		pm.shelfButton(annotation='Info', image1='mup_info.png',
			command='import mup.info; mup.info.gShowInfoWindow()')

		# Debug tools
		if self.mData.mDevelopperMode:
			# Button to reload the mup module
			pm.shelfButton(annotation='Reload', image1='mup_reload.png',
				command='import mup,jos;jos.gReloadModule(mup)')

			# Button to run the tests
			pm.shelfButton(annotation='Test', image1='mup_test.png',
				command='import mup.test;mup.test.gRun()')

		# Set the shelf label
		pm.tabLayout(shelf_top_level, edit=True, tabLabel=[globals.sShelfName,'MUP'])
		
		return True