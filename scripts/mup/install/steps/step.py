import mup

class Step(object):
    """
    Step of the install wizard
    """
    def __init__(self, widget, ui):
        # Copy values from the given widget
        self.mWidget = widget
        self.mUi = ui
        self.mData = mup.gData()
        self.setup()

    def setup(self):
        """Setup this step"""
        pass

    def validate(self):
        """Validate this step
        @return (boolean)"""

        return True

    def execute(self):
        return True

    def next(self):
        if self.execute():
            return self.validate()
        else: return False