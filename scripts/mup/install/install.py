"""
MUP - Maya Unity Pipeline
@description install mup
@author Jos Balcaen
"""
import os
import shutil

from mup.ui.basewindow import BaseWindow
import mup
# Import all steps
from steps import *

from PySide import QtGui, QtCore

#data_object = mup.gData()
#data_object.mProjectFolder = 'C:/Users/Jos/Freework/Games/RaceGame/'


class InstallWindow(BaseWindow):
	"""
	Install wizard window
	"""
	def __init__(self):
		BaseWindow.__init__(self)
		self.mTitle = 'MUP - Install'
		self.mEnableHelp = True
		self.mUiWidgetFile = mup.gData().mWidgetFolder + 'install.ui'
		self.mAllType = 'All'
		self.mDockable = True

		self.mSize = (550, 350)
		self.mStyleSheet = mup.gData().mStyleSheetFile

		self.stepArr = []

	def additionalUiSetup(self):
		# For every tab create a step
		for i in range(self.ui.tabWidget.count()):
			step = None
			tabName = self.ui.tabWidget.widget(i).objectName()
			
			if tabName == "tabWelcome":
				step = StepWelcome(self.ui.tabWidget.widget(i), self.ui)
			elif tabName == 'tabLocation':
				step = StepLocation(self.ui.tabWidget.widget(i), self.ui)
			elif tabName == 'tabUI':
				step = StepInterface(self.ui.tabWidget.widget(i), self.ui)

			if step is None: step = Step(self.ui.tabWidget.widget(i), self.ui)
			# Append step to the array
			self.stepArr.append(step)

		# Set initial state of tab widget
		self.ui.tabWidget.tabBar().hide()
		self.ui.tabWidget.setCurrentIndex(0)

		# Setup general connections
		self.ui.mPushButtonNextQT.clicked.connect(self.next)
		self.ui.mPushButtonPreviousQT.clicked.connect(self.previous)

	def next(self, *args):
		"""
		Callback when user clicks the next button
		"""
		if self.isLatestStep:
			self.close()
			return True

		self.startProgress(inTotal=0)

		result = self.stepArr[self.ui.tabWidget.currentIndex()].next()
		if result:
			self.ui.tabWidget.setCurrentIndex(self.ui.tabWidget.currentIndex()+1)
			mup.gDebug("Next")

		self.stopProgress()
		self.setEnableStateOfPreviousNextButtons()

		return result

	def previous(self, *args):
		"""Callback when user clicks the previous button"""
		if self.stepArr[self.ui.tabWidget.currentIndex()].next():
			self.ui.tabWidget.setCurrentIndex(self.ui.tabWidget.currentIndex()-1)
			mup.gDebug("Previous")

		self.setEnableStateOfPreviousNextButtons()

	@property
	def isLatestStep(self):
		if self.ui.tabWidget.currentIndex() == self.ui.tabWidget.count() - 1: 
			return True
		else: return False

	def setEnableStateOfPreviousNextButtons(self):
		# Privous
		if self.ui.tabWidget.currentIndex() > 0: self.ui.mPushButtonPreviousQT.setEnabled(True)
		else: self.ui.mPushButtonPreviousQT.setEnabled(False)
		# Next
		if self.isLatestStep: 
			self.ui.mPushButtonNextQT.setText("Done")
			self.ui.mPushButtonPreviousQT.setEnabled(False)

def gShow(**kwargs):
	"""Show the install window"""
	global gMupInstallWindow

	try:
		gMupInstallWindow.close()
		gMupInstallWindow.deleteLater()
	except: pass

	gMupInstallWindow = InstallWindow()
	gMupInstallWindow.showUi()
	return gMupInstallWindow
	