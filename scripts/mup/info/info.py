from info_window import InfoWindow

def gShowInfoWindow():
	"""Show info window
	@return (InfoWindow) The created window"""

	w = InfoWindow()
	w.showUi()
	return w