"""
MUP - Maya Unity Pipeline
@description Browser Window
@author Jos Balcaen
"""

import mup
from mup.system import general
from mup.ui.basewindow import BaseWindow

import pymel.core as pm
from PySide import QtGui, QtCore


class InfoWindow(BaseWindow):
	"""Info window"""
	def __init__(self):
		BaseWindow.__init__(self)

		self.mTitle = 'MUP - Info'
		self.mEnableHelp = True
		self.mUiWidgetFile = mup.gData().mWidgetFolder + 'info.ui'
		self.mSize = (550, 350)
