"""
MUP - Maya Unity Pipeline
@description Asset Creator
@author Jos Balcaen
"""
import mup
import mup.objects

import pymel.core as pm

from creator_window import CreatorWindow
import os

class CreatorManager(object):
	"""Class containing al functionality for the creator"""
	def __init__(self):
		self.mData = mup.gData()
		
	def createCategory(self, inName):

		directory = self.mData.mModelFolder + inName

		# Create the directory if not already exist
		if not os.path.exists(directory):
			os.makedirs(directory) 

		return True

	def assetExists(self, inName, inCategory):
		"""Check if there's already an asset with the same name and category"""


	def createAsset(self, inName, inCategory, overWrite=False):
		"""Create a new asset with the given information"""

		# Strip off prefix if needed
		if inName.lower().startswith(self.mData.mPrefixModel.lower()): 
			inName = inName[len(self.mData.mPrefixModel):]

		# Compose the model name
		model_filename = self.mData.mPrefixModel + inName

		# Model folder
		folder = self.mData.mModelFolder + inCategory + '/' + inName + '/'

		# Check if there's already an asset with the given name
		if os.path.exists(folder): 
			result = pm.confirmDialog(title='Already exists', message='An asset already exists with the given name.', button=['Overwrite','Cancel'], defaultButton='Cancel', cancelButton='Cancel', dismissString='Cancel')
			if result == 'Cancel':
				return False

		# Create the folder for the new asset
		if not os.path.exists(folder): 
			os.makedirs(folder)

		# Construct the asset path
		save_path = folder + model_filename

		# Save the current scene in the asset folder
		pm.renameFile(save_path)
		pm.saveFile(type=self.mData.mWorkType)

		# Export the new asset if specified
		asset = mup.objects.Asset(folder)
		asset.author = self.mData.mUser
		asset.export()

		return True

def gShow(**kwargs):
	"""Show the asset creator"""
	global gMupCreatorWindow

	try:
		gMupCreatorWindow.close()
		gMupCreatorWindow.deleteLater()
	except: pass

	manager = CreatorManager()
	gMupCreatorWindow = CreatorWindow(manager)
	gMupCreatorWindow.showUi()

	return gMupCreatorWindow
