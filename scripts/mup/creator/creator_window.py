"""
MUP - Maya Unity Pipeline
@description Create assets
@author Jos Balcaen
@TODO Choose selection/scene
"""
import mup
import mup.browser

from mup.ui.basewindow import BaseWindow
from mup.ui.basewindow import sGetMayaWindow

from PySide import QtGui, QtCore
import pymel.core as pm


class CreatorWindow(BaseWindow):
	"""Window where the user can create a new asset"""
	def __init__(self, inManager):
		BaseWindow.__init__(self)

		self.mManager = inManager
		self.mTitle = 'MUP - Creator'
		self.mEnableHelp = True
		self.mUiWidgetFile = mup.gData().mWidgetFolder +'creator.ui'
		self.mModelFolder = mup.gData().mModelFolder
		self.mSize = (300,100)


	def additionalUiSetup(self):
		"""@derived (BaseWidow)"""
		# Populate mComboBoxTypeQT
		self.mTypes = self.mManager.mData.mCategories
		self.ui.mComboBoxTypeQT.addItems(self.mTypes)

		# Prefill creator
		self.ui.mLineEditNameQT.setText(pm.sceneName().namebase)

		# Set connections
		self.ui.mPushButtonNewTypeQT.clicked.connect(self.createNewCategory)
		self.ui.mPushButtonCreateQT.clicked.connect(self.createAsset)


	def commandAfterShow(self):
		"""
		Command executed after the window is shown
		@derived (BaseWindow)"""
		pass


	def createNewCategory(self, *args):
		"""
		Create a new category
		A popup will be displayed where the user can enter the new category name
		"""
		# Ask the user for specify a category name
		category_name = self.getTextInput("Category name:")
		if category_name is None: return

		self.mManager.createCategory(category_name)

		# Display the new category in the combo box
		self.refreshCategories()
		self.ui.mComboBoxTypeQT.setCurrentIndex(self.mTypes.index(category_name))
		

	def refreshCategories(self):
		"""Repopulate the categories"""

		# Get the categories from the data object
		self.mTypes = mup.gData().mCategories

		# Reset the items in the combobox
		self.ui.mComboBoxTypeQT.clear()
		self.ui.mComboBoxTypeQT.addItems(self.mTypes)


	def getTextInput(self, inLabel="Enter:", inText="", inTitle='MUP'):
		"""
		Get text input from the user
		@param inLabel (string) Text displayed above the line edit. Default: Enter
		@param inText (string) Placeholder text in the line edit. Default: Empty
		@param inTitle (string) Title of the dialog. Default: MUP
		"""
		text, ok = QtGui.QInputDialog.getText(self,inTitle, inLabel, text=inText)
		
		if ok: return text
		else: return None


	def createAsset(self, *args):
		"""Create a new asset with the filled in information
		@TODO Ask user for overwrite if there already an asset with the same name
		@TODO Add checkbox if user wants to export the newly asset
		@TODO Add checkbox if user wants to show the asset in the browser after creation"""

		# Get the assets name
		asset_name = self.ui.mLineEditNameQT.text()
		category = self.ui.mComboBoxTypeQT.currentText()

		# Create the asset
		self.mManager.createAsset(asset_name, category)

		# Close the asset creator
		self.close()

		# Show the asset in the browser
		if self.ui.mCheckBoxOpenInBrowserQT.checkState() == QtCore.Qt.Checked:
			mup.browser.gShow(assetToSelect=asset)

		return True