"""
MUP - Maya Unity Pipeline
@description System module
@author Jos Balcaen
@date 25/10/2014
"""

from log import gDebug, gError, gInfo, gPrint, gWarning, Result, gLogToFile