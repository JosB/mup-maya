"""
MUP - Maya Unity Pipeline
@description Globals
@author Jos Balcaen
@date 30/08/2014
"""

# Static global variables
sExtensionMaya					= 'mb'
sExtensionMayaAscii				= 'ma'
sExtensionFBX					= 'fbx'
sFBXPlugin						= 'fbxmaya.mll'

sExportFormatMaya				= sExtensionMaya
sExportFormatFBX				= sExtensionFBX

sTypeMayaBinary					= 'mayaBinary'

# Default values
sDefaultDebugMode				= True
sDefaultExportFormat			= sExportFormatFBX
sDefaultExportExtension			= sExtensionFBX
sDefaultIconSize				= 100
sDefaultBrowserShowDetails		= True
sDefaultPrefixModel				= 'mod_'
sDefaultPrefixUV				= 'uv_'
sDefaultPrefixThumb				= 'img_'
sDefaultPrefixTexture			= 'tex_'
sDefaultWorkExtension			= sExtensionMaya
sDefaultWorkType				= 'mayaBinary'
sDefaultWorkFolderName			= 'work_folder'
sDefaultGameFolderName			= 'game_folder'
sDefaultMdelFolderPath			= 'Assets/Art/Models/'

sShelfName						= 'gMupShelf'

# Attribute names
sAttrExport						= 'mMupExport'
sAttrExportSave					= 'export'
sAttrCollision					= 'mMupCollision'
sAttrCollisionSave				= 'collision'

sMupMailAdress					= 'josmayascript@gmail.com'