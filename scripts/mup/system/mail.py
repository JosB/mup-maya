"""
MUP - Maya Unity Pipeline
@description Mail
@author Jos Balcaen
@date 12/04/2014
"""
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.mime.text import MIMEText
from email import Encoders

import mup
from mup.system.decorators import gRunInThread

def gSendMail(**kwargs):
	"""
	Send a simple mail
	@param sender (string) Reply address address
	@param recipient (string or array) Address to send to
	@param subject (string) Containing the subject of the mail
	@param body (string) Containing the body of the mail
	@param attachments ([string]) List of files to attach to the mail
	@return (boolean) True if succeeded
	"""
	body = kwargs.setdefault('body', '')
	subject = kwargs.setdefault('subject', 'mup')
	sender = kwargs.setdefault('sender', mup.system.globals.sMupMailAdress)
	recipient = kwargs.setdefault('recipient', mup.system.globals.sMupMailAdress)
	attachments = kwargs.setdefault('attachments', [])

	try:
		# Create smtp object
		smtp_obj = smtplib.SMTP('smtp.gmail.com', 587)
		smtp_obj.ehlo()
		smtp_obj.starttls()
		smtp_obj.login(mup.system.globals.sMupMailAdress.split('@')[0],'sp3s1376')

		msg = MIMEMultipart()
		msg['Subject'] = subject 
		msg['From'] = sender
		msg['To'] = ', '.join(recipient)

		msg = MIMEMultipart('alternative')
		msg.attach(MIMEText(body,'plain' )) # 'html'

		for attachment in attachments:
			part = MIMEBase('application', "octet-stream")
			part.set_payload(open(attachment, "rb").read())
			Encoders.encode_base64(part)

			part.add_header('Content-Disposition', 'attachment; filename="%s"' % attachment)
			msg.attach(part)

		# Send the email
		smtp_obj.sendmail(sender, recipient, msg.as_string() + body)

		# Add feedback message
		mup.gInfo('Successfully sent email')
		return True

	except smtplib.SMTPException:
		mup.gError('Failed to send mail')
		return False


@gRunInThread
def gSendMailInBackground(**kwargs):
	"""
	Send a mail in a separate thread
	Check gSendMail for more information
	"""
	gSendMail(**kwargs)