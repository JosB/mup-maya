"""
MUP - Maya Unity Pipeline
@description Some useful decorators
@author Jos Balcaen
@date 25/10/2014
"""
import mup
from functools import partial
import threading

class gShowProgress(object):
	"""
	Function decorator to show the user (progress) feedback.

	@usage Example:

	import time      
	@gShowProgress(status="Creating cubes...")
	def createCubes():
		num_cubes = 100
		for i in range(num_cubes):
			time.sleep(1)
			if createCubes.isInterrupted(): break
			iCube = cmds.polyCube(w=1,h=1,d=1)
			cmds.move(i,i*.2,0,iCube)
			createCubes.step(float(100)/num_cubes)
	"""
	def __init__(self, status='Busy...', start=0, end=100, interruptable=True):
		import maya.cmds as cmds
		import maya.mel
		
		self.mStartValue = start
		self.mEndValue = end
		self.mStatus = status
		self.mInterruptable = interruptable
		self.mMainProgressBar = maya.mel.eval('$tmp = $gMainProgressBar')
	

	def step(self, inValue=1):
		"""Increase step
		@param inValue (int) Step value"""
		cmds.progressBar(self.mMainProgressBar, edit=True, step=inValue)


	def isInterrupted(self):
		"""Check if the user has interrupted the progress
		@return (boolean)"""
		return cmds.progressBar(self.mMainProgressBar, query=True, isCancelled=True)
		

	def start(self):
		"""Start progress"""
		cmds.waitCursor(state=True)
		cmds.progressBar( self.mMainProgressBar,
				edit=True,
				beginProgress=True,
				isInterruptable=self.mInterruptable,
				status=self.mStatus,
				minValue=self.mStartValue,
				maxValue=self.mEndValue
			)
		cmds.refresh()


	def end(self):
		"""Mark the progress as ended"""
		cmds.progressBar(self.mMainProgressBar, edit=True, endProgress=True)
		cmds.waitCursor(state=False)


	def __call__(self, inFunction):
		"""
		Override call method
		@param inFunction (function) Original function
		@return (function) Wrapped function
		@description
			If there are decorator arguments, __call__() is only called once, 
			as part of the decoration process! You can only give it a single argument, 
			which is the function object.
		"""
		def wrapped_f(*args, **kwargs):
			# Start progress
			self.start()
			# Call original function
			inFunction(*args,**kwargs)
			# End progress
			self.end()
			
		# Add special methods to the wrapped function
		wrapped_f.step = self.step
		wrapped_f.isInterrupted = self.isInterrupted
		
		# Copy over attributes
		wrapped_f.__doc__ = inFunction.__doc__
		wrapped_f.__name__ = inFunction.__name__
		wrapped_f.__module__ = inFunction.__module__

		# Return wrapped function
		return wrapped_f


def gResult(inFunction):
	"""Function decorator used when a function needs to return a rich result (Result)
	The function will have extra member functions available from the result object

	@note As the result object is automatically returned, the inFunction can't return anything by itself
	@param inFunction Function that will be deprecated
	
	@usage Example: @TODO
	"""

	def wrapped_f(*args, **kwargs):
		"""Wrapped function"""

		# Create a result object
		# This object can be controlled by the inFunction by the exposed methods
		# and will be returned after function has been completed
		result = mup.Result()

		# Add result methods to the wrapped function
		wrapped_f.log = result.log
		wrapped_f.setFailed = result.setFailed
		wrapped_f.setSucceeded = result.setSucceeded
		wrapped_f.merge = result.merge

		# Copy over function attributes attributes
		wrapped_f.__doc__ = inFunction.__doc__
		wrapped_f.__name__ = inFunction.__name__
		wrapped_f.__module__ = inFunction.__module__

		# Expose classes to make it easy to log something
		wrapped_f.LogItem = result.LogItem
		wrapped_f.ELogLevel = result.ELogLevel

		# Store the parameters on the result object
		result.mParams = [args, kwargs]
		
		# Call the function
		should_be_none = inFunction(*args, **kwargs)
		assert should_be_none == None, "The original function shouldn't have a return value as this decorator returns the result object"

		# Return the result object
		return result

	return wrapped_f


def gRunInThread(inFunction):
	"""Function decorator used when you want to run a function in a separate thread
	The function returns the thread

	@note This decorator overrides the return value of the function.
	@param inFunction Function that will be deprecated
	
	@usage Example: @TODO
	"""
	def wrapped_f(*args, **kwargs):
		thread = threading.Thread(target=inFunction, args=args, kwargs=kwargs)
		thread.start()
		return thread

	return wrapped_f