"""
MUP - Maya Unity Pipeline
@description Error
@author Jos Balcaen
@date 15/11/2014
@resource 'Practical Maya Programming with Python' by Robert Galanakis
@todo 
	Instead of sending a mail, log exceptions to Jira/Trello
	Attach log information (logs need to be logged to files first
	Attach script editor output
	Capture locals and globals all up the stack
	Make sending information optional (UI)
"""

import sys, platform, os

import maya.utils
import pymel.core as pm

from mup.system import gLogToFile
from mup.system.general import Singleton
import mup

from mup.system.error.error_window import ErrorWindow


def gInstall():
	"""
	Install the MUP exception handler
	@description
		This will install a custom exception handler.
		This exception handles will call the original exception 
		handler when the exception is not from MUP code.
	@return (boolean) True if succeeded
	"""
	exception_handler = ErrorManager()
	return exception_handler.install()


class ErrorManager(Singleton):
	"""Class containing al functionality for handling errors"""
	
	def __init__(self):
		self.mData = mup.gData()
		self.mLogFiles = []


	def writeInfo(self):
		"""
		Write interesting to file
		@return [string] Path to the log files
		"""
		#self.mLogFiles.append([gLogToFile(inCallstack, 'callstack.log'), inCallstack])
		maya_information = self.getMayaInformation()
		self.mLogFiles.append([gLogToFile(maya_information, 'maya_information.log'), maya_information])
		computer_information = self.getComputerInformation()
		self.mLogFiles.append([gLogToFile(computer_information, 'computer_information.log'), computer_information])


	def getMayaInformation(self):
		"""
		Collect Maya information
		@return (string)
		"""

		lines = [
		'Scene Info',
		'    Maya Scene: ' + pm.sceneName(),

		'Maya/Python Info',
		'    Maya Version: ' + pm.about(version=True),
		'    Qt Version: ' + pm.about(qtVersion=True),
		'    Maya64: %s' % pm.about(is64=True),
		'    PyVersion: ' + sys.version,
		'    PyExe: ' + sys.executable,
		]
		return '\n'.join(lines)


	def getComputerInformation(self):
		"""
		Collect computer info
		@return (string)
		"""

		lines = [
		'Machine Info',
		'    OS: ' + pm.about(os=True),
		'    Node: ' + platform.node(),
		'    OSRelease: ' + platform.release(),
		'    OSVersion: ' + platform.version(),
		'    Machine: ' + platform.machine(),
		'    Processor: ' + platform.processor(),

		'Environment Info',
		'    EnvVars'
		]

		# Append the environment variables
		for key in sorted(os.environ.keys()):
			lines.append('        {0:<30}{1}'.format(key, os.environ[key]))

		# Append sys path
		lines.append('    SysPath')
		for path in sys.path:
			lines.append('        ' + path)

		return '\n'.join(lines)


	def normalize(self, inPath):
		"""
		Normalize the given path
		@param inPath (string)
		"""
		import os # Came across some weird case that os was not defined here, so adding it to be sure
		return os.path.normpath(os.path.abspath(inPath))


	def isImportantTb(self, inTb):
		"""
		The author is specified in the init file of this module
		@param inTb (traceback)
		"""

		while inTb:
			code_path = inTb.tb_frame.f_code.co_filename
			code_path = self.normalize(code_path)
			
			module_folder = self.normalize(mup.gData().mModuleFolder)
			
			print code_path
			print module_folder

			if code_path.startswith(module_folder):
				return True

			inTb = inTb.tb_next

		return False


	def clearData(self):
		"""Clear the data of the error handler"""
		self.mLogFiles = []


	def handleOurException(self, inEType, inEValue, inTb, inDetail):
		"""
		Called when a MUP exception occurs
		This will write some information and show a window where the user
		can send a crash report
		@param inEType (type) Exception value
		@param inEValue (Exception) Exception type
		@param inTb (traceback)
		@param inDetail (int)
		@return (string) Call stack
		"""
		call_stack = maya.utils._formatGuiException(inEType, inEValue, inTb, inDetail)
		mup.gPrint(call_stack)

		# Clear the data because it will still contain the information
		# from the previous crash if there was one
		self.clearData()

		# Write useful information to files
		self.writeInfo()

		# Show the error dialog
		window = ErrorWindow(self)
		window.mCallstack = call_stack
		window.mLogFiles = self.mLogFiles
		window.showUi()

		return call_stack


	def exceptHook(self, inEType, inEValue, inTb, inDetail):
		"""Custom except hook
		@param inEType (type) Exception value
		@param inEValue (Exception) Exception type
		@param inTb (traceback)
		@param inDetail (int)
		@return (string) Log message"""
		# Handle the exception with the original except hook
		result = self.mOriginalExceptHook(inEType, inEValue, inTb, inDetail)

		# Handle the exception if it's a mup exception
		if self.isImportantTb(inTb):
			result = self.handleOurException(inEType, inEValue, inTb, inDetail)
		
		return result
	
	
	def install(self):
		"""
		Install the MUP exception handler
		@description
			This will install a custom exception handler.
			This exception handles will call the original exception 
			handler when the exception is not from MUP code.
		@return (boolean) True if succeeded
		"""
		# Check if it's already installed
		if maya.utils.formatGuiException == self.exceptHook:
			mup.gWarning('MUP exception handler already installed.')
			return True

		# Save the original exception hook
		self.mOriginalExceptHook = maya.utils.formatGuiException

		# Set MUP exception hook
		maya.utils.formatGuiException = self.exceptHook