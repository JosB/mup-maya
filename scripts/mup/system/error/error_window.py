"""
MUP - Maya Unity Pipeline
@description Error Window
@author Jos Balcaen
"""

import os, subprocess
from functools import partial
import time

import mup
from mup.system import general
from mup.ui.basewindow import BaseWindow
from mup.ui.accordion_widget import AccordionWidget
from mup.ui.text_edit import TextEdit
from mup.system.mail import gSendMailInBackground
from mup.system.decorators import gRunInThread

import pymel.core as pm
from PySide import QtGui, QtCore


class ErrorWindow(BaseWindow):
	"""Browser window"""
	def __init__(self, inManager):
		BaseWindow.__init__(self)

		self.mManager = inManager
		self.mTitle = 'MUP - Error'
		self.mDisplayStatusBar = False
		self.mEnableHelp = True
		self.mUiWidgetFile = self.mManager.mData.mWidgetFolder + 'error.ui'
		self.mSize = (550, 350)
		self.mDockable = True
		self.mCustomWidgets = [AccordionWidget,TextEdit]

		self.mCallstack = None
		self.mAttachmentData = []


	def additionalUiSetup(self):
		"""Setup UI"""

		self.ui.mDescriptionTextEditQT.setPlaceholderText('Description..')
		self.ui.mLineEditMailAddressQT.setText(self.mManager.mData.mUserMail)

		# Setup the accordion widget
		self.ui.mAccordionWidgetQT = AccordionWidget();

		# Add the crash information
		self.ui.mAccordionWidgetQT.addItem('Crash information', self.buildCrashInformationFrame())

		# Add the files
		for file, text in self.mLogFiles:
			
			item = self.ui.mAccordionWidgetQT.addItem(os.path.basename(file), self.buildAttachmentFrame(file, text))
			item.setCollapsed(True)

		self.ui.mAccordionWidgetQT.setRolloutStyle(self.ui.mAccordionWidgetQT.Maya)
		self.ui.mAccordionWidgetQT.setSpacing(0)# More like Maya but I like some padding.

		self.ui.mVerticalLayoutAccordionWidgetQT.addWidget(self.ui.mAccordionWidgetQT)

		# Signals
		self.ui.mPushButtonSendQT.clicked.connect(self.sendCrashReport)


	def buildAttachmentFrame(self, inFile, inText):
		"""
		Build a frame that displays an attachment
		@param inFile (string) File path
		@param inText (string) Content
		"""
		frame = QtGui.QFrame(self)
		layout = QtGui.QVBoxLayout()
		frame.setLayout(layout)

		# Checkbox
		chb_attach = QtGui.QCheckBox('Attach file')
		chb_attach.setChecked(True)

		# Show in explorer
		btn_show_in_explorer = QtGui.QPushButton('Show in explorer')
		btn_show_in_explorer.setMaximumWidth(100)
		btn_show_in_explorer.clicked.connect(partial(general.gShowFileInExplorer, inFile))

		lbl_content = QtGui.QLabel(inText)
		#label.setStyleSheet('QLabel {color: rgb(255,10,10)}')
		lbl_content.setWordWrap(True)

		h_layout = QtGui.QHBoxLayout()
		h_layout.addWidget(chb_attach)
		h_layout.addWidget(btn_show_in_explorer)

		layout.addLayout(h_layout)
		layout.addWidget(lbl_content)

		# Save the data so we can easily access this when composing the mail
		self.mAttachmentData.append([inFile, chb_attach.checkState])

		return frame

	
	def buildCrashInformationFrame(self):
		"""Build a frame that displays the call stack"""
		frame = QtGui.QFrame(self)
		frame.setLayout(QtGui.QVBoxLayout())

		lbl_content = QtGui.QLabel(self.mCallstack)
		lbl_content.setStyleSheet('QLabel {color: rgb(255,10,10)}')
		lbl_content.setWordWrap(True)

		frame.layout().addWidget(lbl_content)
		return frame


	def commandAfterShow(self):
		"""Command executed after the window is shown"""
		
		pass
		

	def createEditActions(self):
		"""Method to add actions in the edit menu"""
		# @TODO add option to disable crash handler
		pass


	def sendCrashReport(self):
		"""Called when user clicks send"""
		# Compose body text
		body = self.ui.mDescriptionTextEditQT.toPlainText()
		if self.ui.mCheckBoxNotifyMeQT.checkState() == QtCore.Qt.Checked and self.ui.mLineEditMailAddressQT.text() != '':
			self.mManager.mData.mUserMail = self.ui.mLineEditMailAddressQT.text()
			body += '\nPlease notify me when there\'s a fix on %s' % self.ui.mLineEditMailAddressQT.text()

		# Get the attachments
		attachments = []
		if self.ui.mCheckBoxAttachCurrentSceneQT.checkState() == QtCore.Qt.Checked:
			scene_path = os.path.join(mup.gData().mModuleFolder, 'current_scene.mb')
			scene_name = pm.sceneName()
			pm.renameFile(scene_path)
			pm.saveFile()
			pm.renameFile(scene_name)
			attachments.append(scene_path)

		# Add the log files
		for file, checked in self.mAttachmentData:
			if checked():
				attachments.append(file)

		# Send the mail		
		gSendMailInBackground(
			body=body,
			subject='MUP error',
			attachments=attachments
		)

		# Show a thank you message for a second
		label = QtGui.QLabel()
		pixmap = QtGui.QPixmap(mup.gData().mUiFolder + 'thank_you.png')
		label.setPixmap(pixmap)
		label.setWindowFlags(QtCore.Qt.CustomizeWindowHint)
		label.show()

		close_window(label, 1)

		self.close()


@gRunInThread
def close_window(inWindow, inDelay=1):
	"""
	Helper method to close a window with delay
	@param inWindow (QWidget)
	@param inDelay (float)
	"""
	time.sleep(inDelay)
	inWindow.close()