"""
MUP - Maya Unity Pipeline
@description General functionality
@author Jos Balcaen
@date 25/10/2014
"""
import os, shutil, stat, subprocess

from PySide import QtGui, QtCore
import pymel.core as pm

import mup
from mup.system import globals, decorators

def gConvertPathToMupXML(inPath):
	"""
	@param inPath (string) Path of the Maya file
	@return (string) Path to the mup xml file
	"""
	file_name, file_extension = os.path.splitext(inPath)

	return (file_name + '.xml')


def gConvertWorkFileToExportPath(inFile):
	"""
	Convert the given path to the corresponding export path, inclusive extension
	@param inFile (string) Full path to a work file
	@return (string) Full game path
	"""
	file_name, file_extension = os.path.splitext(inFile)

	return gConvertWorkToGame(file_name + '.%s' % mup.gData().mExtensionExport)


def gGonvertFolderToWorkFilePath(inFolder):
	"""
	Get the work files path from the asset folder
	@param inFolder (string) Full path to the asset folder
	@return (string) Full path to the work file located in the folder
	"""
	return [(inFolder + '/' + file) for file in os.listdir(inFolder) if file.endswith(".%s" % mup.gData().mExtensionWork)][0]


def gConvertWorkToGame(inPath):
	"""
	Convert a work path to the game path
	@param inPath (string) Full path to a work file
	@return (string) Full path to the corresponding game file. None if given path is not a work folder
	"""
	# Make sure we have forward slashes
	inPath = inPath.replace('\\', '/')

	# Get the index of the work folder in the path
	work_folder_search_string = '/' + mup.gData().mWorkFolderName + '/'
	folder_index = inPath.find(work_folder_search_string)

	if folder_index < 0:
		mup.gWarning('Cannot convert to game path, file not located in work directory: %s' % inPath)
		return None

	else:
		# Get the index where the work folder ends so we can swap it with the game folder
		i = folder_index + len(work_folder_search_string)
		
		return (mup.gData().mGameFolder + inPath[i:])


def gIsValidCategoryFolder(inFolder):
	"""
	Check if the give folder is a valid category folder
	@description This function is called on all folders in the asset directory
		to determine which folders are category folders
	@param inFolder (string) Full folder path
	@return (boolean) True if valid
	"""
	folder_name = os.path.basename(inFolder)
	if not len(folder_name): return False
	
	try:
		# Folder is only valid if there are no maya files inside (=> assume this is a asset folder)
		if len([name for name in os.listdir(inFolder) if name.endswith('.%s' % mup.gData().mExtensionWork)]):
			return False
	except WindowsError as ex:
		mup.gWarning("isValidModelFolder needs a full path, %s" % ex)
		return False

	folder_name = folder_name.lower().strip()

	# Filter out invalid folder. These are folders that are nested in the category folder
	invalid_folders = ['textures', 'tex', '.mayaswatches', 'ref']
	if folder_name in invalid_folders: return False

	# Passed all checks
	return True


def gIsValidProjectFolder(inFolder):
	"""
	Check if the given folder is a valid project folder
	@param inFolder (string) Full folder path
	@return (boolean) True if valid
	"""
	if len(inFolder) < 3:
		mup.gWarning('Folder needs to be at least 4 characters long: %s' % inFolder)
		return False

	return True


def gSetProjectFolder():
	"""Ask the user to specify the project folder"""

	result = QtGui.QMessageBox.warning(None, "MUP - Warning", "Project folder not set, press ok to specify", QtGui.QMessageBox.Ok, QtGui.QMessageBox.Cancel)
	if result == QtGui.QMessageBox.Ok:
		# Popup a dialog where the user can choose a folder
		folder = QtGui.QFileDialog.getExistingDirectory(None, "Select project folder", "c:/", QtGui.QFileDialog.ShowDirsOnly)
		if folder == None: return
		else:
			folder = folder.replace('\\','/') + '/'
			
			if gIsValidProjectFolder(folder):
				data = mup.gData()
				data.mProjectFolder = folder
				data.save()
				return folder
			else:
				return None
				mup.gError("Not a good folder")
	else:
		mup.gError('Canceled by user')
		return None


def gOpenFile(inPath):
	"""
	Open file with default program
	@note Show file in explorer when shift clicked
	@param inPath (string) Full path to the file
	"""

	# Make sure the file exists
	if not os.path.exists(inPath):
		mup.gWarning('Could not open because file does not exists: %s' % inPath)
		return False
	
	# Check if shift has been pressed
	modifiers = QtGui.QApplication.keyboardModifiers()
	if modifiers == QtCore.Qt.ShiftModifier:
		gShowFileInExplorer(inPath)

	else:
		# If it's a Maya file, we open it in Maya
		if inPath.endswith('.' + globals.sExtensionMaya) or inPath.endswith('.' + globals.sExtensionMayaAscii):
			gOpenScene(inPath)

		# Other files we open them with the standard program
		else:
			gOpenFileFromCommandLine(inPath)


def gShowFileInExplorer(inFile):
	"""
	Run explorer and select file
	@param inFile (sting) Full path
	"""

	if inFile == None or len(inFile) == 0: return False

	# We need back slashes
	inFile = inFile.replace('/','\\')
	command = 'explorer /select, "%s"' % inFile

	subprocess.Popen(command)


def gOpenFileFromCommandLine(inFile):
	"""
	Open a file with default program
	@param inFile (string) Full path
	"""

	if len(inFile) == 0: return False

	os.startfile(inFile)

	return True


def gOpenScene(inPath, **kwargs):
	"""Open a new scene
	@param inPath (string) Full path
	"""

	if gSameFile(pm.sceneName(), inPath):
		mup.gWarning('Scene already open: %s' % inPath)
		return True

	try:
		pm.openFile(inPath)
	except RuntimeError as err:
		# Only capture the unsaved changes exception
		if not "unsaved changes" in str(err).lower():
			raise err

		result = pm.confirmDialog(
			title='MUP - Unsaved changes',
			message='Do you want to save changes?',
			button=['Save', 'Discard'],
			defaultButton='Save',
			cancelButton='Cancel',
			dismissString='Cancel')

		if result == 'Discard': 
			pm.openFile(inPath, force=True)
		elif result == "Save":
			if scene_name == '': pm.mel.SaveSceneAs()
			else: pm.saveFile(scene_name)

			# Open the file after saving
			pm.openFile(inPath)


def gSameFile(inFile1, finFile2):
	"""
	Checks if two file paths are pointing to the same file
	@return (boolean) True if they point to the same file
	"""
	return os.path.abspath(inFile1.lower()) == os.path.abspath(finFile2.lower())


def gIsUnityRunning():
	"""
	Check if unity is running
	@description Checks if unity.exe is in the running processes
	"""
	cmd = 'WMIC PROCESS get Caption,Commandline,Processid'
	proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
	
	for line in proc.stdout:
		if 'unity.exe' in line.lower():
			return True

	return False
	

@decorators.gResult
def gDeleteFolder(inPath, inForce=False):
	"""
	Delete a folder and all of its contents
	@param inPath (string) Absolute path to the folder
	@param inForce (boolean) Make writable
	@return (Result)
	"""

	def onerror(inFunc, inPath, inExecInfo):
		"""
		Error handler for ``shutil.rmtree``.
		@param inFunc (function)
		@param inPath (string)
		@param inExecInfo (tuple) (type, error, traceback)
		@description
			If the error is due to an access error (read only file)
			it attempts to add write permission and then retries.
			If the error is for another reason it re-raises the error.

		@usage shutil.rmtree(path, onerror=onerror)
		@source http://stackoverflow.com/questions/2656322/python-shutil-rmtree-fails-on-windows-with-access-is-denied
		"""
		if inForce and not os.access(inPath, os.W_OK):
			# Is the error an access error ?
			os.chmod(inPath, stat.S_IWUSR)
			inFunc(inPath)
		else:
			raise

	# Check if the folder exists
	if not os.path.exists(inPath):
		gDeleteFolder.addLog(gDeleteFolder.LogItem(label='Folder does not exists: "%s"' % inPath, path=inPath, level=gDeleteFolder.ELogLevel.WARNING))
		return
	
	# Try to remove the folder
	try:
		shutil.rmtree(inPath, onerror=onerror)
	except Exception as ex:
		gDeleteFolder.addLog(gDeleteFolder.LogItem(label=str(ex), level=gDeleteFolder.ELogLevel.ERROR))



# works in Python 2 & 3
class _Singleton(type):
	_instances = {}
	def __call__(cls, *args, **kwargs):
		if cls not in cls._instances:
			cls._instances[cls] = super(_Singleton, cls).__call__(*args, **kwargs)
		return cls._instances[cls]
class Singleton(_Singleton('SingletonMeta', (object,), {})): pass
