"""
MUP - Maya Unity Pipeline
@description 
	Export attributes that can be added to Maya objects
	These will be taken into account when exporting.
	Unity relevant attributes will written to the mup xml file,
	these will be picked up by unity in the post importing stage
@author Jos Balcaen
@date 09/04/2014
"""

import pymel.core as pm

import mup
from mup.system import globals


class MupAttribute(object):
	"""Class to hold all data for an attribute"""
	def __init__(self, **kwargs):

		self.mLabel = kwargs.setdefault('label')
		self.mShortName = kwargs.setdefault('shortName')
		self.mLongName = kwargs.setdefault('longName', self.mShortName )
		self.mType = kwargs.setdefault('type')
		self.mDefaultValue = kwargs.setdefault('defaultValue')
		self.mSaveName = kwargs.setdefault('saveName')

		# Set default values
		if self.mDefaultValue == None:

			# Define default values
			default_values = {
				'bool' : False,
				'string' : '',
				'default' : 0
			}

			if self.mType in default_values:
				self.mDefaultValue = default_values[self.mType]
			else: 
				self.mDefaultValue = default_values['default']


def gGetAttributes():
	"""Get a list of all the mup attributes
	@return ([MupAttribute])"""

	mup_attributes = list()

	# Set to export
	mup_attributes.append(
		MupAttribute(
			label			= 'MUP - Export',
			shortName		= globals.sAttrExport,
			saveName		= globals.sAttrExportSave,
			type			= 'bool',
			defaultValue	= True
		)
	)

	# Mark as collision geometry
	mup_attributes.append(
		MupAttribute(
			label			= 'MUP - Collision Geometry',
			shortName		= globals.sAttrCollision,
			saveName		= globals.sAttrCollisionSave,
			type			= 'bool'
		)
	)

	# Attach a script
	#mup_attributes.append(
	#	MupAttribute(
	#		label			= 'MUP - Attach Script',
	#		shortName		= 'mMupScript',
	#		longName		= 'mMupAttachScript',
	#		type			= 'string'
	#	)
	#)

	return mup_attributes


def gAddExtentionAttributes():
	"""Add the extention attributes
	@note Should be executed once at startup
	"""

	for attr in gGetAttributes():
		mup.gDebug('Add attribute "%s" with default value "%s"' % (attr.mLongName, attr.mDefaultValue))
		if attr.mType == 'string':

			pm.addExtension(
				nodeType		= 'transform',
				shortName		= attr.mShortName,
				longName		= attr.mLongName,
				niceName		= attr.mLabel,
				dataType		= attr.mType
			)	
		else:
			pm.addExtension(
			nodeType		= 'transform',
			shortName		= attr.mShortName,
			longName		= attr.mLongName,
			niceName		= attr.mLabel,
			attributeType	= attr.mType,
			defaultValue	= attr.mDefaultValue
		)

	return True