"""
MUP - Maya Unity Pipeline
@description Logging
@author Jos Balcaen
@date 12/04/2014
"""
import sys, os
from PySide import QtGui, QtCore

import pymel.core as pm

import mup
from mup.system.general import Singleton

LOG_FILE = 'output.log'

class ELogLevel():
	"""Enum class to represent log levels"""
	SUCCES	= 4
	INFO	= 3
	WARNING	= 2
	ERROR	= 1

class EScriptLanguage():
   """Enum class to represents scripting languages"""
   CMDS		= 4
   MEL		= 3
   PYMEL	= 2
   PYTHON	= 1


class LogItem(QtGui.QWidget):
	"""Class to hold all data for a log item"""
	ELogLevel = ELogLevel

	def __init__(self, *args, **kwargs):
		"""Create a new log item
		@param level (ELogLevel) Level of the log. Default: ELogLevel.INFO
		@param error (boolean) Mark the log items as an error. Same as setting level to ELogLevel.ERROR
		@param warning (boolean) Mark the log items as a warning. Same as setting level to ELogLevel.WARNING
		@param success (boolean) Mark the log items as a success. Same as setting level to ELogLevel.SUCCESS

		@param label (string) Label describing the problem

		@param fix (LogFix) Fix for the issue. If given, users can execute this in the dialog
		
		@param node (pm.PyNode|string|[string]) Object concerning this issue. The select method will be called on this object when user selects the item
		@param nodes ([pm.PyNode|string]) See param object

		@param path (string) Path to a file/folder. gShowInExplorer will be called on this path when user selects the item
		@param id (string) Unique id. This is used when we want to get all log items of the same id
		"""
		super(LogItem, self).__init__()

		self.mLevel = kwargs.setdefault('level', ELogLevel.INFO)
		self.mFix = kwargs.setdefault('fix')
		self.mLabel = kwargs.setdefault('label', 'Unspecified')
		self.mObject = kwargs.setdefault('node', kwargs.setdefault('object')) # Object to select when item is clicked
		self.mObjects = kwargs.setdefault('nodes', kwargs.setdefault('objects', []))
		self.mPath = kwargs.setdefault('path')

		self.mId = kwargs.setdefault('id', None)
		
		# Merge to two
		if isinstance(self.mObject, list):
			self.mObjects.extend(self.mObject)
		elif self.mObject:
			self.mObjects.append(self.mObject)

		if kwargs.setdefault('error', False): self.mLevel = ELogLevel.ERROR
		if kwargs.setdefault('warning', False): self.mLevel = ELogLevel.WARNING
		if kwargs.setdefault('success', False): self.mLevel = ELogLevel.SUCCESS
		self.mDisplayObject = kwargs.setdefault('displayObject', False)

		layout = QtGui.QHBoxLayout()
		label = QtGui.QLabel()
		layout.addWidget(label)
		layout.setContentsMargins(1,1,1,1)
		layout.setSpacing(0)

		if self.mFix:
			self.mButton = QtGui.QPushButton("Fix")
			self.mButton.setMaximumSize(30,20)
			layout.addWidget(self.mButton)
			self.mButton.pressed.connect(self.runFix)
			# Add tool tip if description is given
			if self.mFix.mDescription:
				self.mButton.setToolTip(self.mFix.mDescription)
			# Add a "Similar" button if an id is given.
			self.mSimilarButton = None
			if self.mId:
				self.mSimilarButton = QtGui.QPushButton("Similar")
				self.mSimilarButton.setMaximumSize(45,20)
				self.mSimilarButton.setToolTip('Fix similar: %s' % self.mFix.mDescription)
				layout.addWidget(self.mSimilarButton)
				self.mSimilarButton.pressed.connect(self.runAllSimilarFixes)

		self.setLayout(layout)

		log_text = ''
		if self.mLevel == ELogLevel.WARNING:
			log_text += 'Warning\t'
			label.setStyleSheet("QLabel{color: rgb(250, 127, 0)}")
		elif self.mLevel == ELogLevel.ERROR:
			log_text += 'Error\t'
			label.setStyleSheet("QLabel{color: rgb(250, 10, 10)}")
		elif self.mLevel == ELogLevel.SUCCESS:
			log_text += 'Success\t'
			label.setStyleSheet("QLabel{color: rgb(0, 200, 0)}")
		else:
			log_text += 'Info\t'

		if self.mObjects and self.mDisplayObject:
			if isinstance(self.mObjects[0], pm.PyNode):
				object_string = ', '.join([o.name() for o in self.mObjects])
			else:
				object_string = ', '.join(self.mObjects)
			log_text += '[%s] ' % object_string

		log_text += self.mLabel
		label.setText(log_text)

	def runFix(self, *args):
		"""Run the fix of this log item"""
		if not self.mFix: return False
		self.mFix.run()
		self.mButton.setEnabled(False)
		if self.mSimilarButton != None:
			self.mSimilarButton.setEnabled(False)

		return True

	def runAllSimilarFixes(self, *args):
		self.parent().parent().fixAllItemsWithId(id=self.mId)

	@property
	def mIsWarning(self):
		return self.mLevel == ELogLevel.WARNING

	@property
	def mIsError(self):
		return self.mLevel == ELogLevel.ERROR

	@property
	def mIsSucces(self):
		return self.mLevel == ELogLevel.SUCCES

	@property
	def mIsInfo(self):
		return self.mLevel == ELogLevel.INFO

	def log(self):
		"""Log this item"""
		mup.gInfo(str(self))


	def __repr__(self):
		s = "%s %s" % (self.mLevel, self.mLabel)
		if self.mFix:
			s += "\nFix %s" % (self.mFix.mDescription)
		return s


class Result(object):
	"""
	Class that represents a result. 
	Used to return a rich result from a function instead of a boolean
	"""
	LogItem = LogItem # Easy acces to create a LogItem object
	ELogLevel = ELogLevel # Easy acces to set LogLevel

	def __init__(self, *args, **kwargs):
		super(Result, self).__init__(*args, **kwargs)

		self.mResult = ELogLevel.SUCCES
		self.mLogs = []
		self.mParams = [] # Used by de gResult decorator
	

	@property
	def mFailed(self):
		"""
		If the result has the failed status
		@note This will take all the log items into account
		@return (boolean)
		"""
		# Check if we have a failed log item
		for log_item in self.mLogs:
			if log_item.mLevel == ELogLevel.ERROR:
				return True

		return self.mResult == ELogLevel.ERROR


	@property
	def mSucceeded(self):
		"""
		If the result has the success status
		@note This will take all the log items into account
		@return (boolean)
		"""
		# Make sure we don't have a failed log item
		for log_item in self.mLogs:
			if log_item.mLevel == ELogLevel.ERROR:
				return False

		return self.mResult != ELogLevel.ERROR


	def setFailed(self):
		"""
		Set the result as failed
		@return (Result) This result
		"""
		self.mResult = ELogLevel.ERROR

		return self


	def setSucceeded(self):
		"""
		Set the result as succeeded
		@return (Result) This result
		"""
		self.mResult = ELogLevel.SUCCES

		return self


	def addLog(self, inLogItem):
		"""
		Log an item to this result object
		@param inLogItem (LogItem)
		"""
		assert isinstance(inLogItem, self.LogItem)

		self.mLogs.append(inLogItem)
		
		return self


	def merge(self, inResult):
		"""
		Merge the given result with this result
		@description The log items will added and the result value will be overwritten if it's worse
		@param inResult (Result)
		@return (Result) The updated result
		"""
		# Merge the two logs
		self.mLogs.extend(inResult.mLogs)

		# If the result of the inResult is worse then this object's result, overwrite it
		if inResult.mResult < self.mResult:
			self.mResult = inResult.mResult
		
		return self


	def errorsToWarning(self):
		"""
		Convert the errors to warnings
		@return (Result) This result
		"""
		# Check log items
		for i in range(len(self.mLogs)):
			if self.mLogs[i].mLevel == ELogLevel.ERROR:
				self.mLogs[i].mLevel = ELogLevel.WARNING

		# Check result value
		if self.mResult == ELogLevel.ERROR:
			self.mResult = ELogLevel.WARNING

		return self


	def show(self):
		"""Show the log items
		@return (LogWindow)"""

		from mup.system.log.log_window import LogWindow

		# Create and show the window
		d = LogWindow(self.mLogs)
		d.showUi()

		return d


	def log(self, inText=None):
		"""Log this this result object"""
		
		for log in self.mLogs:
			log.log()


	def __repr__(self):
		"""String representation"""
		return "Result object with %s log items and status: %s" % (self.mLogs, "Success" if self.mSucceeded else "Failed")


def gInstall():
	#@TODO clear log file
	log_file = os.path.join(mup.gData().mModuleFolder, LOG_FILE)

	# Create if doesn't exists. Truncates the file
	with open(log_file, 'w+'):
		pass
	pass


def gInfo(inMessage):
	"""Log an info message
	@param inMessage (string)"""
	gPrint(inMessage)


def gError(inMessage):
	"""Log an error message
	@param inMessage (string)"""
	message = 'mup: %s\n' % str(inMessage)
	pm.error(message)
	gLogToFile(message)


def gWarning(inMessage):
	"""Log a warning message
	@param inMessage (string)"""
	message = 'mup: %s\n' % str(inMessage)
	pm.warning(message)
	gLogToFile(message)


def gPrint(inMessage):
	"""Log a message
	@param inMessage (string)"""
	message = 'mup: %s\n' % str(inMessage)
	print message
	gLogToFile(message)


def gDebug(inMessage):
	"""Log a debug message
	@param inMessage (string)"""
	if mup.gData().mDebugMode:
		message = 'mup: %s\n' % str(inMessage)
		sys.stdout.write(message)
		gLogToFile(message)


def gLogToFile(inText, inFile=LOG_FILE):
	"""Log a message/text to a file
	@param inText (string)
	@param inFile (string) Path to the log file"""

	if not os.path.isabs(inFile):
		inFile = os.path.join(mup.gData().mModuleFolder, inFile)

	with open(inFile, "a+") as f:
		f.write(inText)

	return inFile


def gLogEnvironment():
	"""Log the environment"""

	pass