"""
BuildingBlockExporter - logger

@description Dialog to show log items from a BuildingBlock
@author Jos Balcaen
@date 29/04/2014
"""
from mup.ui.basewindow import BaseWindow
from PySide import QtGui, QtCore
import pymel.core as pm

from mup.system import general

class QLogListWidgetItem(QtGui.QListWidgetItem):
	"""Placeholder item"""
	def __init__(self, parent=None):
		super(QLogListWidgetItem, self).__init__(parent)

		# self.runFix = None # Method
		self.__source = None

	def setSource(self, source):
		self.__source = source

	def __getattr__(self, name):
		if self.__source:
			try:
				return getattr(self.__source, name)
			except AttributeError as a_err: 
				mup.gDebug(a_err)
		# Default behavior


class QLogListWidget(QtGui.QListWidget):
	"""QListWidget to display log items"""
	def __init__(self, inParent=None, inLogArr=[]):
		"""
		Create a new QLogListWidget
		@param inLogArr ([LogItem])
		"""
		super(QLogListWidget, self).__init__(inParent)
		self.mLogArr = inLogArr
		self.mDeselect = False

		# Log the given logItems
		for log in self.mLogArr:
			self.addLog(log)

		# Connect when user clicks an item
		self.currentItemChanged.connect(self.logItemClicked)
		self.itemClicked.connect(self.logItemClicked)

		# Set the style sheet to set the background of selected items to gray instead of blue. This is more readable.
		self.setStyleSheet('QListWidget:item:selected {background: rgb(70,70,70);}')


	def logItemClicked(self, *args):
		"""
		Method executed when user selects a log item
		Default behavior : select the object(s)
		"""
		item = self.currentItem()
		
		# Get the object from the item
		if item is not None:
			existing_nodes = []
			for object in item.mObjects:
				if pm.objExists(object): 
					existing_nodes.append(object)

			if existing_nodes:  pm.select(existing_nodes, r=True)
			else: pm.select(cl=True)

			if item.mPath:
				general.gShowFileInExplorer(item.mPath)

		# Deselect the item again
		if self.mDeselect:
			self.setItemSelected(item, False)


	def keyPressEvent(self, event):
		"""When user presses a key when this widget is active
		@overridden"""
		# Get the pressed key
		key = event.key()

		# Add hotkeys
		if key == QtCore.Qt.Key_F:
			item = self.currentItem()
			if item: item.runFix()
		else:
			# Call keyPressEvent of parent to avoid losing other key press functionality (like up-down arrow)
			super(QLogListWidget, self).keyPressEvent(event)


	def addLog(self, inLogItem):
		"""Log an item to the view"""
		item = QLogListWidgetItem()
		item.setSource(inLogItem)
				
		# Add the ListWidgetItem to the QListView
		self.addItem(item)
		# Set the size of our LogItem
		item.setSizeHint(inLogItem.sizeHint())
		# Replace the ListWidgetItem with our LogItem
		self.setItemWidget(item, inLogItem);


	def fixAllItemsWithId(self, id):
		items_with_id = []
		for i in range(self.count()):
			if id == self.item(i).mId:
				items_with_id.append(self.item(i))

		for item in items_with_id:
			item.runFix()


	def clearLogs(self):
		"""Remove all log items"""
		self.clear()

		
class LogWindow(BaseWindow):
	def __init__(self, inLogArr):
		BaseWindow.__init__(self)

		self.mTitle = 'MUP Logs'
		self.mWindow = 'MUPLogWindow'
		self.mSize = (800,300)
		self.mLogArr = inLogArr
		self.mDisplayMenuBar = False
		self.mDisplayStatusBar = False


	def additionalUiSetup(self):
		"""Method to do additional ui stuff"""

		self.mainVLayout.setAlignment(QtCore.Qt.AlignTop)
		
		# Create a log view
		self.mLogList = QLogListWidget(inLogArr=self.mLogArr)
		self.mainVLayout.addWidget(self.mLogList)

		# Create a close button
		btn_close = QtGui.QPushButton('Close')
		btn_close.clicked.connect(self.close)
		
		self.mainVLayout.addWidget(btn_close)
