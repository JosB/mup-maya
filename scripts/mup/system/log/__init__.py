"""
MUP - Maya Unity Pipeline
@description Log module
@author Jos Balcaen
@date 31/01/2015
"""
from log import gDebug, gError, gInfo, gPrint, gWarning, Result, gLogToFile, LogItem, gInstall