"""
MUP - Maya Unity Pipeline
@description Options saved in a ini file
@author Jos Balcaen
@date 11/04/2014
"""
import ConfigParser
import os

import mup

SECTION	= 'MUP' # Section in the ini file

class Option(object):
	"""Class that represents an option"""
	mName			= ""
	mValue			= ""
	mDefaultValue	= ""


class LazyOptions(object):
	"""Lazy options
	@note This will only write the used options that don't have there default value"""

	def __init__(self):

		self.mOptions = Options()
		self.mOptionDict = {}


	def save(self):
		"""Write the data to the option file"""

		for option in self.mOptionDict.values():

			if not self.mOptions.exists(option.mName.lower(), inSection=SECTION):
				# Skip the options that have their default value
				if option.mValue == option.mDefaultValue:
					mup.gDebug("Skip %s = %s, same as default" % (option.mName, option.mValue))
					continue

			mup.gDebug("Save %s = %s" % (option.mName, option.mValue))
			self._writeOption(option)

		mup.gWarning("Saved data")


	def reload(self):
		"""Reload the option object by clearing the cache"""
		self.mOptionDict = {}


	def reset(self):
		"""Reset all values to the default values"""
		for option in self.mOptionDict.values():
			option.mValue = option.mDefaultValue


	def get(self, inName, inDefault=None):
		"""
		Get a value from the options dict
		@param inName (string) Key
		@param inDefault (string/int/boolean) If not exists, this value will be returned
		@return (string/int/boolean) Value from the options dict
		"""
		try:
			return self.mOptionDict[inName.lower()].mValue
		except KeyError:
			return self._addOption(inName.lower(), inDefault)
			 

	def set(self, inName, inValue):
		"""
		Set a value from the options dict
		@param inName (string) Key
		@param inValue (string/int/boolean) Value
		"""
		try:
			self.mOptionDict[inName.lower()].mValue = inValue
		except KeyError:
			return self._addOption(inName.lower(), inValue)


	def _addOption(self, inName, inValue):
		"""
		Add a new option to the list
		@param inName (string) Name of the new option
		@param inValue (object) Value for the new option
		@return (object) Value of the option
		"""

		option = Option()
		option.mName = inName
		option.mValue = inValue
		option.mDefaultValue = inValue

		# Read if already exists
		option.mValue = self._readOption(option)

		self.mOptionDict[inName.lower()] = option

		return option.mValue


	def _writeOption(self, inOption):
		"""
		Set the value of an option
		@param option (Option)
		"""
		# Add the option if it doesn't exists
		if not self.mOptions.exists(inOption.mName.lower(), inSection=SECTION):
			self.mOptions.add(
				inName			= inOption.mName.lower(),
				inDefaultValue	= inOption.mDefaultValue,
				inValue			= inOption.mValue, 
				inSection		= SECTION
			)
		else: self.mOptions.set(inOption.mName.lower(), inOption.mValue, inSection=SECTION)


	def _readOption(self, inOption):
		"""
		Get the value of an option
		@param inOption (Option)
		"""
		# If the option doesn't exists, return the default value
		if not self.mOptions.exists(inOption.mName.lower(), inSection=SECTION):
			return inOption.mDefaultValue
		else: return self.mOptions.get(inOption.mName.lower(), inSection=SECTION)


class Options(object):
	"""
	Options object to manage the mup options
	@note all options are saved to an ini file
	"""
	def __init__(self):

		self.mOptionsFile = os.environ['APPDATA']+'\\mup\\MUPOptions.ini'

		self.__createIfNotExists(self.mOptionsFile)

		self._config = ConfigParser.ConfigParser()
		self._config.read(self.mOptionsFile)
		self._defaultSection = 'global'
		self._default = '_default'

		# Create default section if it doesn't exist
		if (not self._config.has_section(self._defaultSection)):
			self._config.add_section(self._defaultSection)
		

	def __createIfNotExists(self, inPath):
		"""
		Create options file if it doesn't exists
		@param inPath (string) Path to an options file
		"""

		# Create the directory for the file if it doesn't exists
		if not os.path.exists(os.path.dirname(inPath)):
			os.makedirs(os.path.dirname(inPath))

		# Create the file if it doesn't exists
		if not os.path.exists(inPath):
			try:
				file = open(inPath, 'w')
				file.close()
			except IOError:
				mup.gError('Could not create Maya options file at the given location! %s' % inPath)

		
	def __getDefaultNameFromName(self, inName):
		"""
		Get the default name for the given option name
		@param inName (string) Option name
		@return (string) Default name
		"""
		return (inName + self._default)


	def add(self, inName, inValue, **kwargs):
		"""
		Add a new option
		@param inName (string)
		@param inValue (object)
		@param inDefaultValue () normal value if not specified
		@param inSection (string) default section if not specified
		"""
		inDefaultValue = kwargs.setdefault('inDefaultValue', inValue)
		inSection = kwargs.setdefault('inSection', self._defaultSection)

		# Read the file
		self._config.read(self.mOptionsFile)
		# Create the section if it doesn't exists
		if not self._config.has_section(inSection):
			self._config.add_section(inSection)

		# Set values
		self._config.set(inSection, inName, inValue)
		self._config.set(inSection, self.__getDefaultNameFromName(inName), inDefaultValue)

		# Save changes to disk
		with open(self.mOptionsFile, 'w') as config_file:
			self._config.write(config_file)


	def remove(self, inName, **kwargs):
		"""
		Remove an option
		@param inName (string)
		@param inSection (string) default section if not specified
		"""
		inSection = kwargs.setdefault('inSection', self._defaultSection)

		# Read the file
		self._config.read(self.mOptionsFile)
		# Remove the option
		self._config.remove_option(inSection, inName)

		# Save changes to disk
		with open(self.mOptionsFile, 'w') as config_file:
			self._config.write(config_file)


	def exists(self, inName, **kwargs):
		"""
		Check if an options exists
		@param inName (string)
		@param inSection (string) default section if not specified
		@return (boolean)
		"""
		inSection = kwargs.setdefault('inSection', self._defaultSection)
		
		# Read the file
		self._config.read(self.mOptionsFile)
		# Check if the option exists
		return (self._config.has_option(inSection, inName))
		

	def get(self, inName, **kwargs):
		"""
		Get the value of an option
		@param inName (string)
		@param inSection (string) default section if not specified
		@return (type) None if the option doesn't exists
		"""
		inSection = kwargs.setdefault('inSection', self._defaultSection)
		
		# Read the file
		self._config.read(self.mOptionsFile)
		
		# Return the value as the correct type
		if (self._config.has_option(inSection, inName)):
			# Eval the value, if this fails just get the value as a string
			try: return eval(self._config.get(inSection, inName))
			except Exception: return self._config.get(inSection, inName)
		
		return None


	def getDefault(self, inName, **kwargs):
		"""
		Get the default value of an option
		@param inName (string)
		@param inSection (string) default section if not specified
		"""
		inSection = kwargs.setdefault('inSection', self._defaultSection)
		
		# Get the default name
		default_name = self._getDefaultNameFromName(inName)

		# Read the file
		self._config.read(self.mOptionsFile)

		# Return the value as the correct type
		if (self._config.has_option(inSection, default_name)):
			# Eval the value, if this fails just get the value as a string
			try: return eval(self._config.get(inSection, default_name))
			except Exception: return self._config.get(inSection, default_name)

		return None


	def set(self, inName, inValue, **kwargs):
		"""
		Set the value of an option
		@param inName (string)
		@param inValue (type)
		@param inSection (string) default section if not specified
		"""
		inSection = kwargs.setdefault('inSection', self._defaultSection)

		# Read the file
		self._config.read(self.mOptionsFile)
		# Set the value
		self._config.set(inSection, inName, inValue)

		# Save changes to disk
		with open(self.mOptionsFile, 'w') as config_file:
			self._config.write(config_file)


	def setDefault(self, inName, inValue, **kwargs):
		"""
		Set the default value of an option
		@param inName (string)
		@param inValue (type)
		@param inSection (string) default section if not specified
		"""
		inSection = kwargs.setdefault('inSection', self._defaultSection)

		# Convert it to the default name
		default_name = self._getDefaultNameFromName(inName)
		
		# Read the file
		self._config.read(self.mOptionsFile)
		# Set the default value
		self._config.set(inSection, default_name, inValue)

		# Save changes to disk
		with open(self.mOptionsFile, 'w') as config_file:
			self._config.write(config_file)
