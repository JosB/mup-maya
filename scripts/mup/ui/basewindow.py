"""
MUP - Maya Unity Pipeline
@description Base class for a tool window (PySide)
@author Jos Balcaen
@date 25/10/2014
"""
import os
from shiboken import wrapInstance
import xml.etree.ElementTree as xml
from cStringIO import StringIO
import logging
import webbrowser

import pysideuic
import maya.OpenMayaUI as mui
from PySide import QtGui, QtCore, QtUiTools

import mup.system.mail
from mup.system import globals


UI_FILE = mup.gData().mWidgetFolder + 'basewindow.ui'
UI_REQUEST_FILE = mup.gData().mWidgetFolder + 'basewindow_requestwidget.ui'
ICON_FILE = 'mup_icon.png'


def sGetMayaWindow():
	"""
	Get the Maya main window as a QMainWindow instance
	@return (QMainWindow) Maya window
	"""
	ptr = mui.MQtUtil.mainWindow()
	return wrapInstance(long(ptr), QtGui.QMainWindow)


def gLoadUiType(inUiFile):
	"""
	Load a Qt Designer .ui file and return a tuple of the generated form class and the Qt base class.
	These can then be used to create any number of instances of the user interface 
	without having to parse the .ui file more than once.

	@param inUiFile (string) the file name or file-like object containing the .ui file.
	@return (form, QWidget) form class and the PySide base class
	"""
	# PySide lacks the "loadUiType" command, so we have to convert the ui file to py code in-memory first
	# and then execute it in a special frame to retrieve the form class.
	
	# Suppress pysideuic logging spam
	pyqt_loggers = ['pysideuic.properties','pysideuic.uiparser'] 

	for log in pyqt_loggers: 
		logger = logging.getLogger(log) 
		logger.setLevel(logging.WARNING)

	# Parse the file with ElemenTree
	parsed = xml.parse(inUiFile)
	widget_class = parsed.find('widget').get('class')
	out_form_class = parsed.find('class').text
	
	with open(inUiFile, 'r') as f:
		o = StringIO()
		frame = {}
			
		pysideuic.compileUi(f, o, indent=0)
		pyc = compile(o.getvalue(), '<string>', 'exec')
		exec pyc in frame
			
		# Fetch the base class and form class based on their type in the xml from designer
		out_form_class = frame['Ui_%s' % out_form_class]
		out_base_class = eval('QtGui.%s' % widget_class)
			
	return out_form_class, out_base_class


def loadUiWidget(inUiFile, inParent=None, inCustomWidgets=[]):
	"""
	Loads a form from the given .ui file and creates a new widget
	with the given parentWidget to hold its contents.
	@param inUiFile (string) File name or file-like object containing the .ui file.
	@param inParent (QWidget)
	@return (@TODO)
	"""
	
	ui_file = QtCore.QFile(inUiFile)
	ui_file.open(QtCore.QFile.ReadOnly)

	loader = QtUiTools.QUiLoader()

	for widget in inCustomWidgets:
		loader.registerCustomWidget(widget)

	out_ui = loader.load(ui_file, inParent)

	ui_file.close()

	return out_ui


# Load the ui file, and create my class
form_class, base_class = gLoadUiType(UI_FILE)

class BaseWindow(form_class, base_class):

	@classmethod
	def showUi(cls):
		"""A function to instantiate the base window"""
		win = cls()
		win.create()
		return win


	def __init__(self, inParent = sGetMayaWindow()):
		"""A base window with a demo set of ui widgets"""
		#init our ui using the MayaWindow as parent
		super(BaseWindow, self).__init__(inParent)
		
		self.mWindow = 'baseToolWindow'
		self.mTitle = "Base Qt Window"
		self.mSize = (546, 350)
		self.mShowProgressBar = False
		self.mEnableHelp = False
		self.mDisplayStatusBar = True
		self.mDisplayProgressBar = False
		self.mDockable = False
		self.mUiWidgetFile = None
		self.mProgressBar = None
		self.mainVLayout = None # Store main layout in this variable
		self.mStyleSheet = None

		self.mCustomWidgets = []

		# @TODO close window if it already exists


	def showUi(self):
		"""Draw window"""

		self.setupUi(self)
		self.setWindowTitle(self.mTitle)
		self.setWindowIcon(QtGui.QIcon(ICON_FILE))
		self.setObjectName(self.mWindow)
		self.mainVLayout = self.mVerticalLayout

		if self.mUiWidgetFile != None:
			# uic adds a function to our class called setupUi, calling this creates all the widgets from the .ui file
			self.ui = loadUiWidget(self.mUiWidgetFile, inCustomWidgets=self.mCustomWidgets)
			# add widget to the main layout
			self.mVerticalLayout.addWidget(self.ui)

		if self.mDisplayStatusBar:
			if self.mDisplayProgressBar: self.createProgressBar()
		else:
			# Hide status bar
			self.statusBar().hide()
		

		# Additional ui setup (make connections,..)
		self.additionalUiSetup()
		# Create edit actions
		self.createEditActions()

		# Setup menu entries
		self.actionHelp.setText('Help on %s' % self.mTitle)
		self.actionHelp.setStatusTip('Help on %s' % self.mTitle)
		self.actionHelp.triggered.connect(self.helpMenuCmd)
		self.actionHelp.setEnabled(self.mEnableHelp)
		
		self.actionSubmitBug.setStatusTip('Submit bug for %s' % self.mTitle)
		self.actionSubmitBug.triggered.connect(self.submitBugMenuCmd)

		self.actionRequestFeature.setStatusTip('Request feature for %s' % self.mTitle)
		self.actionRequestFeature.triggered.connect(self.requestFeatureMenuCmd)

		self.statusBar().showMessage('Ready')
		self.resize(self.mSize[0], self.mSize[1])
		if self.mStyleSheet is not None:
			if os.path.exists(self.mStyleSheet):
				with open (self.mStyleSheet, "r") as myfile:
					data=myfile.read()
					self.setStyleSheet(data)

		self.show()
		self.commandAfterShow()


	def createProgressBar(self):
		"""Create progress bar"""
		self.mProgressBar = QtGui.QProgressBar()
		self.mProgressBar.setMaximumSize(9999999,10)
		self.statusBar().addPermanentWidget(self.mProgressBar)


	def startProgress(self, inTotal=100, inMessage="Busy"):
		"""
		Start the progress
		@param inTotal (int) Maximum value of the progress bar
		@param inMessage (string) Message to be displayed next to the progress bar
		"""
		self.statusBar().showMessage(inMessage)

		# Create a new progress bar if not already exists
		if self.mProgressBar == None:
			self.mProgressBar = QtGui.QProgressBar()
			self.mProgressBar.setMaximumSize(9999999,10)
			self.mProgressBar.setMaximum(inTotal)
			self.statusBar().addPermanentWidget(self.mProgressBar)

		self.mProgressBar.setValue(0)

	def updateProgress(self, inValue=1, inAdd=True, inMessage="Busy"):
		"""
		Update the progress
		@param inValue (int) New progress value. Default: 1
		@param inAdd (boolean) When True, the given value will be added. Default: True
		@param inMessage (string) Message to be displayed next to the progress bar
		"""
		self.setStatusMessage(inMessage)

		if self.mProgressBar != None:
			if inAdd:
				inValue += self.mProgressBar.value()
			if inValue <= self.mProgressBar.maximum():
				self.mProgressBar.setValue(inValue)


	def stopProgress(self, inRemove=True, inMessage='Ready'):
		"""
		Stop the progress
		@param inRemove (boolean) Remove the progress bar. Default: True
		@param inMessage (string) Message to be displayed next to the progress bar
		"""
		self.setStatusMessage(inMessage)

		if inRemove:
			self.statusBar().removeWidget(self.mProgressBar)
			self.mProgressBar = None

	def setStatusMessage(self, inMessage='Ready'):
		"""
		Set status message
		@param inMessage (string)
		"""
		self.statusBar().showMessage(inMessage)


	def createEditActions(self):
		"""
		Method to add actions in the edit menu
		@example
			exitAction = QtGui.QAction(QtGui.QIcon('exit.png'), '&Close', self)        
			exitAction.setShortcut('Ctrl+Q')
			exitAction.setStatusTip('Close %s' % self.mTitle)
			exitAction.triggered.connect(self.close)

		self.menuEdit.addAction(exitAction)
		"""
		self.menuEdit.setEnabled(False)


	def additionalUiSetup(self):
		"""Method to setup the ui"""
		
		pass


	def helpMenuCmd(self, *args):
		"""Method to display custom help, override in inheriting classes for custom help"""
		
		webbrowser.open('http://www.josbalcaen.com/')


	def requestFeatureMenuCmd(self, *args):
		"""Method to request a feature"""

		window = sendRequestMailWindow(toolname=self.mTitle,type='FEATURE')
		window.showUi()


	def submitBugMenuCmd(self, *args):
		"""Method to request a feature"""

		window = sendRequestMailWindow(toolname=self.mTitle,type='BUG')
		window.showUi()


	def commandAfterShow(self):
		"""This method is called after the window has been shown"""
		pass


class sendRequestMailWindow(BaseWindow):
	"""Class for request bug/feature window"""

	#@TODO select the text in the description when clicked on it

	def __init__(self, toolname='',type='BUG'):
		BaseWindow.__init__(self)

		self.mTitle = 'Submit a %s for %s' % (type,toolname)
		self.mToolName = toolname
		self.mwindow = 'requestPyQtWindow'
		self.mRequestMailAdress = globals.sMupMailAdress
		self.mType = type
		self.mUiWidgetFile = UI_REQUEST_FILE
		self.mDisplayStatusBar = False
	

	def additionalUiSetup(self):
		"""Do the additional ui setup"""

		self.ui.btnSubmit.clicked.connect(self.submitBtnCmd)


	def submitBtnCmd(self, *args):
		"""@called When user presses the submit button"""

		sender			= self.ui.lineEditFrom.displayText()
		subject			= '%s: %s' % (self.mType, self.ui.lineEditSubject.displayText())
		body			= "%s\n%s" % (self.mToolName, self.ui.textEditDescription.toPlainText())
		body			= 'Submitted by: %s\n%s' % (mup.gData().mUser, body)
		reponse_subject = 'You submitted the following request: %s' % subject
		reponse_body	= 'Thank you for submitting the following request:\n\n%s' % body

		# Send the mail to the mup address
		try:
			mup.system.mail.gSendMail(sender=sender, subject=subject, body=body)
		except Exception as ex:
			QtGui.QMessageBox.information(self,"Failed","The following error occurred:\n%s" % ex)
			mup.gError('%s Failed to send the request.' % ex)
			return

		# Send a confirmation mail to the sender
		try:
			mup.system.mail.gSendMail(recipient=sender, subject=reponse_subject, body=reponse_body)
		except Exception as ex:
			QtGui.QMessageBox.information(self,"Warning","Successfully sent the mail to MUP, but Failed to sent a confirmation mail to you.\nYou probably filled in a wrong mail address")
			mup.gWarning('%s Failed to send the request.' % ex)
			self.close()
			return

		# Display dialog and close window
		QtGui.QMessageBox.information(self,"Succeeded",reponse_body)
		self.close()

"""
class ExampleWindow(BaseWindow):
	def __init__(self):
		BaseWindow.__init__(self)
		self.mTitle = 'ExampleWindow'
		self.mEnableHelp = True
		#self.mUiWidgetFile = 'C:/Users/jos.balcaen/Desktop/widget.ui'
		self.mDisplayProgressBar = True

	def additionalUiSetup(self):
		model = QtGui.QFileSystemModel()
		model.setRootPath("C:/Users/jos.balcaen/Desktop/");
		#self.ui.treeView.setModel(model)

ex = ExampleWindow()
ex.showUi()
"""