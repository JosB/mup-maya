"""
MUP - Maya Unity Pipeline
@description Custom TextEdit that has a placeholder text
@author Jos Balcaen
@date 31/01/2015
@source Refactored from an online example
"""

from PySide import QtCore, QtGui
 
class TextEdit(QtGui.QTextEdit):
	"""Text Edit with placeholder text"""

	def __init__( self, inParent ):
		QtGui.QTextEdit.__init__(self, inParent)

		self.mPlaceholderText = ''

	def setPlaceholderText(self, inText):
		"""@param inText (string)"""
		self.mPlaceholderText = inText

		if len(self.toPlainText()) == 0:
			self.setHtml('<font color=\"#808080\"><i>%s</i></font>' % self.mPlaceholderText)

	def focusInEvent(self, inEvent):
		"""@param inEvent (QFocusEvent)"""
		text = self.toPlainText();

		if len(text) != 0 and text == self.mPlaceholderText:
				self.clear()
		
		# Call base class event
		QtGui.QTextEdit.focusInEvent(self, inEvent)

	def focusOutEvent(self, inEvent):
		"""@param inEvent (QFocusEvent)"""
		if len(self.mPlaceholderText) != 0:
			if len(self.toPlainText()) == 0:
				self.setHtml('<font color=\"#808080\"><i>%s</i></font>' % self.mPlaceholderText)

		# Call base class event
		QtGui.QTextEdit.focusOutEvent(self, inEvent)
