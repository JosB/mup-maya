"""
MUP - Maya Unity Pipeline
@description Browser Window
@author Jos Balcaen
"""

import os, subprocess

import mup
from mup.objects import AssetWidgetItem
from mup.system import general
from mup.ui.basewindow import BaseWindow
from mup.ui.flowlayout import FlowLayout

import pymel.core as pm
from PySide import QtGui, QtCore


class BrowserWindow(BaseWindow):
	"""Browser window"""
	def __init__(self, inManager):
		BaseWindow.__init__(self)

		self.mManager = inManager
		self.mTitle = 'MUP - Browser'
		self.mEnableHelp = True
		self.mUiWidgetFile = self.mManager.mData.mWidgetFolder + 'browser.ui'
		self.mViewDetails = self.mManager.mData.mShowAssetDetails
		self.mViewMenu = QtGui.QMenu("View")
		self.mSize = (550, 350)
		self.mDockable = True

	def additionalUiSetup(self):
		"""Setup UI"""
		# Populate mComboBoxTypeQT
		self.ui.mComboBoxTypeQT.addItems(self.mManager.mTypes)

		# Set connections
		self.ui.mComboBoxTypeQT.currentIndexChanged.connect(self.refresh)
		self.ui.mLineEditSearchQT.textChanged.connect(self.lineEditSearchChanged)

		self.ui.mListWidgetAssetsQT.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.ui.mListWidgetAssetsQT.customContextMenuRequested.connect(self.showMenu)
		self.ui.mListWidgetAssetsQT.itemClicked.connect(self.itemSelected)
		self.ui.mListWidgetAssetsQT.itemDoubleClicked.connect(self.itemDoubleClicked)
		icon_size = self.mManager.mData.mIconSize
		self.ui.mListWidgetAssetsQT.setIconSize(QtCore.QSize(icon_size,icon_size))

		self.ui.mHorizontalLayoutSelectedQT.setAlignment(QtCore.Qt.AlignLeft)

		# Create the view menu
		self.createViewMenu()


	def commandAfterShow(self):
		"""Command executed after the window is shown"""
		
		self.startProgress()
		self.refresh()
		self.stopProgress()
		

	def createViewMenu(self):
		"""Method to add actions in the edit menu"""
		
		show_details_action = QtGui.QAction(QtGui.QIcon('exit.png'), '&Show asset details', self) 
		show_details_action.setCheckable(True)
		show_details_action.setChecked(self.mViewDetails)
		show_details_action.setShortcut('Ctrl+D')
		show_details_action.setStatusTip('Show details of selected asset')
		show_details_action.triggered.connect(self.showDetailsClicked)

		self.mViewMenu.addAction(show_details_action)
		self.menuBar().addMenu(self.mViewMenu)


	def showDetailsClicked(self, *args):
		"""Called when user clicks menu checkbox"""
		
		self.mViewDetails = not self.mViewDetails
		
		self.mManager.mData.mShowAssetDetails = self.mViewDetails
		self.mManager.mData.save()
		
		selected_items = self.ui.mListWidgetAssetsQT.selectedItems()
		self.itemSelected(selected_items[0] if len(selected_items) != 0 else None)


	def itemDoubleClicked(self, inAssetItem):
		"""
		Called when user double clicks an item
		@param inAssetItem (AssetWidgetItem)
		"""

		inAssetItem.open()


	def itemSelected(self, inAssetItem):
		"""
		Called when an item has been selected
		@param inAssetItem (AssetWidgetItem)
		"""
		
		# Clear the detail layout
		self.clearLayout(self.ui.mHorizontalLayoutSelectedQT)

		if inAssetItem is None: return

		# Show the details of the selected asset
		if self.mViewDetails:
			# Get the thumb file
			if os.path.exists(inAssetItem.mThumbFile):
				pixmap = QtGui.QPixmap(inAssetItem.mThumbFile)
			else:
				pixmap = QtGui.QPixmap(mup.gData().mUiFolder +'icon.png')

			# Scale the pixmap
			pixmap = pixmap.scaled(mup.gData().mIconSize,mup.gData().mIconSize, aspectRatioMode =QtCore.Qt.KeepAspectRatioByExpanding)
		
			lbl_icon = QtGui.QLabel()
			lbl_icon.setPixmap(pixmap)
			
			lbl_name = QtGui.QLabel("Name: %s" % inAssetItem.mName)
			lbl_author = QtGui.QLabel("Created by: %s" % inAssetItem.mAuthor)
			lbl_last_modified = QtGui.QLabel("Last modified: %s" % inAssetItem.mLastModifiedDate)
			lbl_last_modified_author = QtGui.QLabel("By: %s" % inAssetItem.mLastModifiedAuthor)
			lbl_tags = QtGui.QLabel("Tags: %s" % ', '.join(inAssetItem.mTags))

			lbl_info = QtGui.QLabel()
			if inAssetItem.mInfo:
				lbl_info.setText("Info: %s" % inAssetItem.mInfo)
			lbl_info.setAlignment(QtCore.Qt.AlignTop)

			v_layout = QtGui.QVBoxLayout()
			v_layout.addWidget(lbl_name)
			v_layout.addWidget(lbl_author)
			v_layout.addWidget(lbl_last_modified)
			v_layout.addWidget(lbl_last_modified_author)
			v_layout.addWidget(lbl_tags)
		
			# Add the icon and the information
			self.ui.mHorizontalLayoutSelectedQT.addWidget(lbl_icon)
			self.ui.mHorizontalLayoutSelectedQT.addLayout(v_layout)
			self.ui.mHorizontalLayoutSelectedQT.addWidget(lbl_info)
			

	def clearLayout(self, inLayout):
		"""
		Helper function to clear a layout
		@param inLayout (QLayout)
		@recursive if nested layouts
		"""

		if inLayout.count() == 0: return
		while inLayout.count():
			child = inLayout.takeAt(0)
			if child.widget() is not None:
				child.widget().deleteLater()
			elif child.layout() is not None:
				self.clearLayout(child.layout())


	def showMenu(self, inPosition):
		"""
		Show the context menu with relevant actions for the selected asset(s)
		@param inPosition (QPoint)
		"""

		selected_assets = self.ui.mListWidgetAssetsQT.selectedItems()

		one = len(selected_assets) == 1
		zero = len(selected_assets) == 0
		if zero: this_file = False
		else: this_file = general.gSameFile(pm.sceneName(),selected_assets[0].mMayaFile)

		# Create all the actions
		open_action = QtGui.QAction("Open", self)
		open_action.triggered.connect(self.openItemClicked)
		
		import_action = QtGui.QAction("Import @TODO" if one else "Import selected @TODO", self)
		import_action.triggered.connect(self.importItemClicked)

		export_action = QtGui.QAction("Export" if one else "Export selected", self)
		export_action.triggered.connect(self.exportItemClicked)

		show_action = QtGui.QAction("Show in work files", self)
		show_action.triggered.connect(self.showWorkFileInExplorerClicked)

		show_game_action = QtGui.QAction("Show in game files", self)
		show_game_action.triggered.connect(self.showExportFileInExplorerClicked)

		grab_thumb_action = QtGui.QAction("Grab thumb", self)
		grab_thumb_action.triggered.connect(self.grabThumbOfSelected)

		edit_asset_action = QtGui.QAction("Edit", self)
		edit_asset_action.triggered.connect(self.editAssetClicked)

		delete_asset_action = QtGui.QAction("Delete" if one else "Delete selected", self)
		delete_asset_action.triggered.connect(self.deleteAssetClicked)

		# Create menu
		# Note: only add the relevant actions to the menu
		menu = QtGui.QMenu(self)

		if one: menu.addAction(open_action)
		if not zero: menu.addAction(import_action)
		if not zero: menu.addAction(export_action)
		
		menu.addSeparator()
		
		if not zero and this_file: menu.addAction(grab_thumb_action)

		menu.addSeparator()

		if one: menu.addAction(show_action)
		if one: menu.addAction(show_game_action)

		menu.addSeparator()

		if not zero: menu.addAction(delete_asset_action)
		if one: menu.addAction(edit_asset_action)


		# Only show the menu if it has actions
		if len(menu.findChildren(QtGui.QAction)) > 0:
			menu.exec_(self.ui.mListWidgetAssetsQT.mapToGlobal(inPosition))


	def openItemClicked(self):
		"""Open the the work file of the selected item"""

		selected_assets = self.ui.mListWidgetAssetsQT.selectedItems()

		if len(selected_assets) == 0: 
			mup.gError("No asset selected")
			return False

		if len(selected_assets) > 1:
			mup.gWarning("More than one asset selected, only open the last one")

		# Open the last selected asset
		selected_assets[-1].open()
	

	def importItemClicked(self):
		"""Import the selected assets"""

		selected_assets = self.ui.mListWidgetAssetsQT.selectedItems()
		self.startProgress(inTotal=len(selected_assets), inMessage="Importing (1/%s)" % len(selected_assets))
		
		# Import every asset
		for i in range(len(selected_assets)):
			selected_assets[i].importAsset(inRestoreScene=False)
			self.updateProgress(inMessage="Importing (%s/%s)" % (i+1,len(selected_assets)))

		self.stopProgress()


	def exportItemClicked(self):
		"""Export the selected assets"""

		# Save the scene so we can restore it after export
		original_scene = pm.sceneName()

		selected_assets = self.ui.mListWidgetAssetsQT.selectedItems()
		self.startProgress(inTotal=len(selected_assets), inMessage="Exporting (1/%s)" % len(selected_assets))
		
		# Export every asset
		for i in range(len(selected_assets)):
			selected_assets[i].export(inRestoreScene=False)
			self.updateProgress(inMessage="Exporting (%s/%s)" % (i+1,len(selected_assets)))

		# Restore the scene
		if not general.gSameFile(pm.sceneName(), original_scene):
			general.gOpenScene(original_scene, askForSave=False)

		self.stopProgress()


	def lineEditSearchChanged(self, inText):
		"""
		Filter the assets in the list widget
		@param inText (string)
		"""

		#@TODO add delay and interruption of previous filter process to make sure typing into the filter box is smooth
		self.filterAssets()


	def refresh(self):
		"""
		Refresh the assets
		@description calls populateAssets()
		"""
		
		self.populateAssets()
		self.filterAssets()


	def filterAssets(self):
		"""Filter the assets"""

		# Get the filter text
		filter = self.ui.mLineEditSearchQT.displayText().lower()

		if len(filter) == 0:
			# When no filter is specified, show all assets
			for i in range(self.ui.mListWidgetAssetsQT.count()):
				self.ui.mListWidgetAssetsQT.item(i).setHidden(False)
		else:
			# When there's a filter filter specified, hide everything and unhide filtered assets
			for i in range(self.ui.mListWidgetAssetsQT.count()):
				self.ui.mListWidgetAssetsQT.item(i).setHidden(True)

			filtered_results = []

			for i in range(self.ui.mListWidgetAssetsQT.count()):
				asset = self.ui.mListWidgetAssetsQT.item(i)

				# Assign filer points to the assets so we can sort by relevance
				points = 0
				if filter in asset.mName.lower(): points+=1
				if filter in asset.mAuthor.lower(): points+=1
				if filter in asset.mLastModifiedAuthor.lower(): points+=1
				for tag in [tag.lower() for tag in asset.mTags]: 
					if filter in tag:
						points += 1

				# Only add the assets if it got points
				if points:
					filtered_results.append([points, asset])

			#@TODO find out how to order the items
			for result in filtered_results:
				#self.ui.mListWidgetAssetsQT.addItem(filtered_results[1])
				result[1].setHidden(False)

		# Sort the items
		self.ui.mListWidgetAssetsQT.sortItems(QtCore.Qt.AscendingOrder)


	def populateAssets(self):
		"""Populate all the assets and add them to the list widget"""

		# Clear all the assets
		self.ui.mListWidgetAssetsQT.clear()

		# If there are no types, we assume there are no assets
		if len(self.mManager.mTypes) == 0:
			return

		# Get the folder to get the items from
		selected_type = self.mManager.mTypes[self.ui.mComboBoxTypeQT.currentIndex()]
		# If 'all' is selected, set the selected_type to an empty string, so the model folder is used
		if selected_type == self.mManager.mAllType: 
			selected_type = ''
		self.currentFolder = self.mManager.mData.mModelFolder + selected_type
		
		# Get all the assets in this folder
		asset_folders = self.mManager.getAllAssetsInFolder(self.currentFolder)
		
		# Add all assets to the list widget
		for asset_folder in asset_folders:                                                                    
			self.ui.mListWidgetAssetsQT.addItem(AssetWidgetItem(asset_folder))


	def grabThumbOfSelected(self):
		"""Take a screen grab for the selected asset"""
		
		selected_assets = self.ui.mListWidgetAssetsQT.selectedItems()
		for asset in selected_assets:
			asset.grabViewport()


	def showWorkFileInExplorerClicked(self):
		"""Show selected assets in explorer"""
		
		selected_assets = self.ui.mListWidgetAssetsQT.selectedItems()
		for asset in selected_assets:
			general.gShowFileInExplorer(asset.mMayaFile)


	def showExportFileInExplorerClicked(self):
		"""Show exported of selected assets in explorer"""
		
		selected_assets = self.ui.mListWidgetAssetsQT.selectedItems()
		for asset in selected_assets:
			general.gShowFileInExplorer(asset.mExportFile)


	def editAssetClicked(self):
		"""Open the asset editor for the selected assets"""

		selected_assets = self.ui.mListWidgetAssetsQT.selectedItems()
		for asset in selected_assets:
			self.mManager.openAssetInEditor(asset)


	def deleteAssetClicked(self):
		"""Delete the selected assets"""

		selected_assets = self.ui.mListWidgetAssetsQT.selectedItems()

		# Ask for confirmation
		result = pm.confirmDialog(
			title='Confirm', message='You sure you want to delete %s' % ', '.join([asset.mName for asset in selected_assets]),
			button=['Yes','Cancel'], defaultButton='Yes', cancelButton='Cancel', dismissString='Cancel'
		)
		if result == 'Cancel':
			return False

		for asset in selected_assets:
			result = asset.delete()

			if result.mSucceeded:
				self.ui.mListWidgetAssetsQT.takeItem(self.ui.mListWidgetAssetsQT.row(asset))
				result.log()
			else:
				result.show()


	def selectAsset(self, inAsset):
		"""
		Select the given asset in the browser
		@param inAsset (Asset)
		"""

		for i in range(self.ui.mListWidgetAssetsQT.count()):
			
			if general.gSameFile(self.ui.mListWidgetAssetsQT.item(i).mFolder, inAsset.mFolder):
				self.ui.mListWidgetAssetsQT.setCurrentItem(self.ui.mListWidgetAssetsQT.item(i))
				self.itemSelected(self.ui.mListWidgetAssetsQT.item(i))
				self.ui.mListWidgetAssetsQT.scrollTo(self.ui.mListWidgetAssetsQT.indexFromItem(self.ui.mListWidgetAssetsQT.item(i)))
				return
		
		mup.gError("Item to select not found: %s" % inAsset)