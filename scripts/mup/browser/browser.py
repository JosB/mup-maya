"""
MUP - Maya Unity Pipeline
@description Browser
@author Jos Balcaen
"""
import mup

import os
import pymel.core as pm
import subprocess

from browser_window import BrowserWindow
import mup.editor as editor

class BrowserManager(object):
    """Class containing all functionality for the browser"""

    def __init__(self):
        self.mData = mup.gData()
        self.mAllType = 'All'


    @property
    def mTypes(self):
        """Get all the types"""
        if not os.path.exists(self.mData.mModelFolder or ''):
            mup.gWarning('Model folder does not exist: "%s"' % self.mData.mModelFolder)
            return []
        all_types =  [self.mAllType] + [name for name in os.listdir(self.mData.mModelFolder) if os.path.isdir(os.path.join(self.mData.mModelFolder, name))]
        return all_types


    def getAllAssetsInFolder(self, inFolder):
        """
		Get all assets recursively in a folder
        @param inFolder (string)
        @return ([string]) Asset folders
        @recursive
		"""

        assets = []

        for root, dirs, files in os.walk(inFolder):
            for file in files:
                if file.endswith(".mb"):
                    file_path = os.path.join(root, file).replace('\\','/')
                    folder = os.path.dirname(file_path)
                    folder_name = os.path.basename(folder)
                    if 'mod_%s.' % folder_name.lower() in file.lower():
                        assets.append(folder)

        return assets


    def openAssetInEditor(self, inAsset):
        """
		Open the given asset in the asset editor
        @param inAsset(Asset)
		"""

        editor.gShowAssetEditor(inAsset)


def gShow(**kwargs):
    """
	Show the browser
    @param assetToSelect (Asset) If specified, the given asset will be selected
	"""
    select_asset = kwargs.setdefault('assetToSelect')

    manager = BrowserManager()
    w = BrowserWindow(manager)
    w.showUi()
    
    if select_asset is not None:
        w.selectAsset(select_asset)
