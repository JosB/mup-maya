"""
MUP - Maya Unity Pipeline
@description Data
	All the global data is managed in the data object.
	The object is synced with an option file.
	The option file is saved in appdata similar to this path; 
	C:\Users\jos\AppData\Roaming\mup\MUPOptions.ini

@author Jos Balcaen
@date 09/04/2014
"""

import mup
from mup.system.options import LazyOptions
from mup.system import globals
from mup.system.general import Singleton

import os, getpass
from PySide import QtGui, QtCore


class __Data(Singleton):
	"""
	Data object
	@derived Singleton Will only be created once
	"""
	def __init__(self):
		# Options are saved in a lazy options object
		self.mOptions = LazyOptions()

		# The user is set the first time the data object is created
		# We do it this way to able to change the user for debugging/testing
		self.__user = getpass.getuser()

	def save(self):
		"""Write the data to the option file"""
		self.mOptions.save()

		mup.gInfo("Saved data")


	"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	Properties saved in the option file
	Note: Name dependencies in the editor_window.py
	"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	def get_debugMode(self): return self.mOptions.get('DebugMode', globals.sDefaultDebugMode)
	def set_debugMode(self, value): self.mOptions.set('DebugMode', value)
	mDebugMode = property(get_debugMode, set_debugMode)

	def get_exportFormat(self): return self.mOptions.get('ExportFormat', globals.sDefaultExportFormat)
	def set_exportFormat(self, value): self.mOptions.set('ExportFormat', value)
	mExportFormat = property(get_exportFormat, set_exportFormat)

	def get_exportExtension(self): return self.mOptions.get('ExtensionExport', globals.sDefaultExportExtension)
	def set_exportExtension(self, value): self.mOptions.set('ExtensionExport', value)
	mExtensionExport = property(get_exportExtension, set_exportExtension)

	def get_workExtension(self): return self.mOptions.get('ExtensionWork', globals.sDefaultWorkExtension)
	def set_workExtension(self, value): self.mOptions.set('ExtensionWork', value)
	mExtensionWork = property(get_workExtension, set_workExtension)
	
	def get_iconSize(self): return self.mOptions.get('IconSize', globals.sDefaultIconSize)
	def set_iconSize(self, value): self.mOptions.set('IconSize', value)
	mIconSize = property(get_iconSize, set_iconSize)

	def get_showAssetDetails(self): return self.mOptions.get('ShowAssetDetails', globals.sDefaultBrowserShowDetails)
	def set_showAssetDetails(self, value): self.mOptions.set('ShowAssetDetails', value)
	mShowAssetDetails = property(get_showAssetDetails, set_showAssetDetails)

	def get_projectFolder(self, openDialogIfNotExists=True):
		"""Get the project folder. @TODO: If not set, show the install dialog. Except when install"""
		return self.mOptions.get('ProjectFolder')

	def set_projectFolder(self, value):
		# Make sure we save the path with forward slashes and it ends with a slash
		value = value.replace('\\','/')
		if not value.endswith('/'): value += '/'
		self.mOptions.set('ProjectFolder', value)
	mProjectFolder = property(get_projectFolder, set_projectFolder)

	def get_workFolderName(self): return self.mOptions.get('WorkFolderName', globals.sDefaultWorkFolderName)
	def set_workFolderName(self, value): self.mOptions.set('WorkFolderName', value)
	mWorkFolderName = property(get_workFolderName, set_workFolderName)

	def get_gameFolderName(self): return self.mOptions.get('GameFolderName', globals.sDefaultGameFolderName)
	def set_gameFolderName(self, value): self.mOptions.set('GameFolderName', value)
	mGameFolderName = property(get_gameFolderName, set_gameFolderName)

	def get_modelFolderPath(self): return self.mOptions.get('ModelFolderPath', globals.sDefaultMdelFolderPath)
	def set_modelFolderPath(self, value): self.mOptions.set('ModelFolderPath', value)
	mModelFolderPath = property(get_modelFolderPath, set_modelFolderPath)

	def get_prefixModel(self): return self.mOptions.get('PrefixModel', globals.sDefaultPrefixModel)
	def set_prefixModel(self, value): self.mOptions.set('PrefixModel', value)
	mPrefixModel = property(get_prefixModel, set_prefixModel)

	def get_prefixThumb(self): return self.mOptions.get('PrefixThumb', globals.sDefaultPrefixThumb)
	def set_prefixThumb(self, value): self.mOptions.set('PrefixThumb', value)
	mPrefixThumb = property(get_prefixThumb, set_prefixThumb)

	def get_workType(self): return self.mOptions.get('WorkType', globals.sDefaultWorkType)
	def set_workType(self, value): self.mOptions.set('WorkType', value)
	mWorkType = property(get_workType, set_workType)

	def get_exportXML(self): return self.mOptions.get('ExportXML', True)
	def set_exportXML(self, value): self.mOptions.set('ExportXML', value)
	mExportXML = property(get_exportXML, set_exportXML)

	def get_userMail(self): return self.mOptions.get('UserMail', None)
	def set_userMail(self, value): self.mOptions.set('UserMail', value)
	mUserMail = property(get_userMail, set_userMail)

	"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	Properties (not directly saved on disk)
	"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	
	def getCategories(self, **kwargs):
		"""
		Get all the available categories
		@param recursive (boolean) Inclusive subdirectories. If False only root categories will be returned. Default: True
		@param relative (boolean) Display relative path from the model folder. Default: True
		@return ([string])
		"""
		recursive = kwargs.setdefault('recursive', True)
		relative = kwargs.setdefault('relative', recursive)

		category_list = []
		model_folder = self.mModelFolder

		if recursive:
			dirs = [x[0] for x in os.walk(model_folder)]
			for dir in dirs:
				if not mup.system.general.gIsValidCategoryFolder(dir): continue
				base_name = os.path.basename(dir)
			
				dir = dir.replace('\\', '/')
			
				if relative:
					category_list.append(dir[len(model_folder):].replace('/',' / '))
				else:
					category_list.append(base_name)
		else:
			for name in os.listdir(model_folder):
				# Only collect folders
				if os.path.isdir(os.path.join(model_folder, name)):
					category_list.append(name)

		return category_list

	# Create property for getting the default categories
	mCategories = property(getCategories)

	@property
	def mModuleFolder(self):
		"""
		Get the path of the mup folder
		@example (string) C:/Users/jos/Documents/freework/Scripts/Maya/mup/
		"""
		return '/'.join(os.path.abspath(__file__).split(os.sep)[:-3]) + '/'

	@property
	def mMupModuleFolder(self):
		return self.mModuleFolder + 'scripts/mup/'

	@property
	def mUiFolder(self):
		return self.mMupModuleFolder + 'ui/'

	@property
	def mWidgetFolder(self):
		"""Folder where all ui files are stored"""
		return self.mMupModuleFolder + 'widgets/'

	@property
	def mStyleSheetFile(self):
		return self.mUiFolder + 'stylesheet.css'

	@property
	def mWorkFolder(self):
		if not self.mProjectFolder: return None
		if not self.mWorkFolderName: return None
		return self.mProjectFolder + self.mWorkFolderName + '/'
	
	@property
	def mGameFolder(self):
		if not self.mProjectFolder: return None
		if not self.mGameFolderName: return None
		return self.mProjectFolder + self.mGameFolderName + '/'

	@property
	def mModelFolder(self):
		if not self.mWorkFolder: return None
		return self.mWorkFolder + self.mModelFolderPath

	def get_user(self): return self.__user
	def set_user(self, value):
		"""Set the user. This should only be used for developer reasons"""
		self.__user = value
		gWarning('The user has been changed to "%s", please report if this is not intentionally')
	mUser = property(get_user, set_user)

	@property
	def mDevelopperMode(self):
		return self.mUser.lower() in ('jos') # @HARDCODED


	def validate(self):
		"""
		Check if all necessary data exists and is valid in this data object
		@return (boolean) True if everything is valid
		@TODO
		"""
		
		# Check if folders exists
		if self.mProjectFolder == None or not os.path.exists(self.mProjectFolder) or \
			self.mWorkFolder == None or not os.path.exists(self.mWorkFolder) or \
			self.mGameFolder  == None or not os.path.exists(self.mGameFolder) or \
			self.mModelFolder == None or not os.path.exists(self.mModelFolder): 
				return False
		return True


def gData():
	"""
	Get the data object. 
	@note This object is only created once.
	@return (__Data)
	"""

	return __Data()

