"""
MUP - Maya Unity Pipeline
@description File executed at Maya startup, start point of this mup module
@author Jos Balcaen
@date 11/07/2014
"""

import sys, os, inspect, logging
import pymel.core as pm
from pymel import mayautils

# Print a startup message to the output window
print r"""
------------------------------------------------------------------------------------
  _  _ _  _ ___  
  |\/| |  | |__] 
  |  | |__| |      v1.0
  
------------------------------------------------------------------------------------
"""


def gInitMup():
	"""
	Initialize MUP
	- Install the custom exception handler
	- Add the extension attributes
	"""

	# Install the MUP exception handler
	import mup.system.error
	mup.system.error.gInstall()

	import mup.system.log
	mup.system.log.gInstall()

	# Add the extension attributes
	import mup.system.attributes
	mup.system.attributes.gAddExtentionAttributes()

	import mup

	# Check if the data object is valid, otherwise show the install window
	data = mup.gData()
	if not data.validate():
		mup.gInstall()

	mup.gDebug('userSetup complete')


# Execute the initialization
mayautils.executeDeferred( gInitMup )
